import numpy as np
import plotly.graph_objects as go


class Surrogate(object):

    def __init__(self, data, func, nvar=1, var1=None, var2=None, var3=None, deg=None):
        self.data = data
        self.func = func
        self.var1 = var1
        self.var2 = var2
        self.var3 = var3
        self.deg = deg
        self.nvar = nvar

    def ncoff(self):
        """"""
        return (self.deg + 1) ** self.nvar

    def calc_coff(self):
        x = np.linspace(0, 1, 20)
        y = np.linspace(0, 1, 20)
        z = np.linspace(0, 1, 20) 
        X, Y, Z = np.meshgrid(x, y, z, copy=False)
        T = X ** 2 + Y ** 2 + Z ** 2 + np.random.rand(*X.shape) * 0.01

        X = X.flatten()
        Y = Y.flatten()                                        
        Z = Z.flatten()

        A = np.array(self.get_3Dmatrix(X, Y, Z)[0]).T
        B = T.flatten()

        coeff, r, rank, s = np.linalg.lstsq(A, B)
        print coeff

        return coeff

    def get_3Dmatrix(self, X, Y, Z):
        col = []
        rep = []

        for i in range(0, self.deg + 1):
            for j in range(0, self.deg + 1):
                for k in range(0, self.deg + 1):
                    col.append(X ** i * Y ** j * Z ** k)
                    rep.append('X^' + str(i) + '* Y^' + str(j) + '* Z^' + str(k))
        print rep
        return col, rep


    def get_2Dmatrix(self, X, Y, Z):
        pass

    def get_1Dmatrix(self, X, Y, Z):
        pass


if __name__ == '__main__':
    sur = Surrogate(1, 2, 3, 4, deg=2)
    sur.calc_coff()
