"""
Generic class for Solvers
"""


class Solver(object):
    """
    Generic Solver Class
    """
    def __init__(self):
        pass

    def preprocessor(self):
        self.generatemesh()
        self.apply_ic()
        self.apply_bc()

    @property
    def is_stable_solution(self):
        return True

    def generatemesh(self):
        pass

    def apply_ic(self):
        pass

    def apply_bc(self):
        pass

    def solve(self):
        pass

    def postprocessor(self):
        self.animate()
        self.plot()

    def plot(self):
        pass

    def animate(self):
        pass


class Solver1D(Solver):
    pass


class Solver2D(Solver):
    pass

