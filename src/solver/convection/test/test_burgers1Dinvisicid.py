from unittest import TestCase

from ..convection import Burgers1DInvisid


class TestHBurgers1DInviscid(TestCase):

    def setUp(self):
        self.heat1D_obj = Burgers1DInvisid('convection/test/resources/input.xml')

    def test_solve(self):
        self.heat1D_obj.solve()

    def test_plot(self):
        pass