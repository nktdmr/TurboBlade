from unittest import TestCase

from ..convection import Convection1D



class TestConvection1D(TestCase):

    def setUp(self):
        self.heat1D_obj = Convection1D('convection/test/resources/input.xml')

    def test_solve(self):
        self.heat1D_obj.solve()

    def test_plot(self):
        pass
