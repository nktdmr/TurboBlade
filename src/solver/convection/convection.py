import numpy as np
import time
import pylab as pl

from src.cli.cli import print_progress_bar as ppb
from src.cli.xml_parser import XMLParser
from matplotlib import animation, cm
from mpl_toolkits.mplot3d import Axes3D


class Convection1D(object):
    """
    This is a heat conduction class which solve by heat conduction equation
    i.e. dT/dt = alpha * d^2 T/ dx^2
    """
    def __init__(self, input_file):
        """

        Args:
            input_file:
        """
        self.inputs = XMLParser(input_file)
        self.root = self.set_root()

    def set_root(self):
        root = self.inputs.get_nodes('./solver/convection/wave/one_dimensional')
        return root

    @property
    def is_stable_solution(self):
        return True

    @property
    def window_title(self):
        return 'Wave equation 1D'

    @property
    def dpi_settings(self):
        dpi = self.inputs.get_value('./animation_dpi', self.root)
        return dpi

    @property
    def animation_file(self):
        file_name = self.inputs.get_value('./animation_file_name', self.root)
        return file_name

    @property
    def xlabel(self):
        return "Length of Medium in X-Direction in Meters"

    @property
    def ylabel(self):
        return "Velocity in m/sec"

    @property
    def save_animation(self):
        save_anim = self.inputs.get_value('./save_animate', self.root)
        return save_anim

    def solve(self):
        """
        Solve the 1D heat conduction equation
        Returns:

        """

        xn = self.inputs.get_value('./npx', self.root)
        total_time = self.inputs.get_value('./time', self.root)
        dt = self.inputs.get_value('./time_step', self.root)
        self.dt = dt
        tn = int(total_time / dt)
        U = np.ndarray(shape=[xn, tn], dtype=float, order='c')

        start = time.clock()

        U.fill(0.0)
        length = self.inputs.get_value('./length', self.root)
        dx = length / (xn - 1)

        U, ic = self.apply_bc(U, xn, dx)

        max_u = max(ic)
        min_u = min(ic)

        c = self.inputs.get_value('./velocity', self.root)


        print ('\n')
        ppb(0, tn - 1, prefix='Progress:', suffix='Complete', length=75)

        # noinspection PyCompatibility
        for j in xrange(1, tn):
            # noinspection PyCompatibility
            for i in xrange(1, xn):
                c = self.get_velocity(U, i, j)
                U[i, j] = U[i, j - 1] - (c * (dt / dx)) * (
                    U[i, j - 1] - U[i - 1, j - 1])
                if i == xn - 1:
                    U[0, j] = U[i, j]
            ppb(j, tn - 1, prefix='Progress:', suffix='Complete', fill="#")



        print(time.strftime("Time Taken: %H:%M:%S", time.gmtime(time.clock() -
                                                                start)))

        speed = self.inputs.get_value('./animate_speed', self.root)
        self.animate_graph(length, xn, U, tn, min_u, max_u, speed)

    def apply_bc(self, U, xn, dx):

        print ("Applying Boundary Conditions . . . . .   . . . . . . . . . . .")

        ic = []
        for i in xrange(0, xn):
            U[i, 0] = np.sin(i * dx)
            ic.append(U[i, 0])

        print ("\nBoundary Condition Applied . . . . . . . . . . . . . . . .")
        return U, ic


    def get_velocity(self, U, j, i):
        c = self.inputs.get_value('./velocity', self.root)
        return c


    def animate_graph(self, length, npts, T, tn, initial, final, speed):
        """
        This method animates the graph of Temperature vs Length with respect
        to time
        Args:
            length: Length of the material
            npts: Number of points in X direction - Equidistance
            T: Temperatures as results
            tn: Number of time steps
            initial: Initial in_temperature of the material
            final: Heat Flux added to the material as boundary condition
            speed: Speed of the animation

        Returns:

        """

        fig = pl.figure()
        fig.canvas.set_window_title(self.window_title)
        ax = fig.add_subplot(111, autoscale_on=False, xlim=((0 - 0.1 * length),
                                                            length * 1.1),
                             ylim=(initial * 0.9, final * 1.1))
        ax.grid()
        dx = length / (npts-1)
        x = np.arange(0, length+dx, dx)
        line, = ax.plot([], [], 'o-', c='red')
        time_template = 'time = %.1fs'
        time_text = ax.text(0.05, 0.9, '', transform=ax.transAxes)
        n_frames = int((tn - 1) / speed)

        def animate(i):
            y = T[:, i * speed]
            line.set_data(x, y)
            time_text.set_text(time_template % (i * self.dt * speed))
            #fig.savefig("result/{}.png".format(str(i * speed)))
            ppb(i, n_frames-1, prefix='Progress:', suffix='Complete', fill="#")
            return line, time_text

        def init():
            line.set_data([], [])
            time_text.set_text('')
            return line, time_text

        # Init only required for blitting to give a clean slate.

        print ('\n')
        ani = animation.FuncAnimation(fig, animate,
                                      frames=n_frames,
                                      interval=1,
                                      init_func=init,
                                      blit=True)
        ax.grid(linestyle='--')
        pl.xlabel(self.xlabel)
        pl.ylabel(self.ylabel)
        pl.title(self.window_title)

        if self.save_animation:
            print("Saving Animation. This may Take a while.....\n")
            start = time.clock()
            ani.save(self.animation_file, writer=animation.FFMpegWriter(fps=1),
                     dpi=self.dpi_settings)
            print (time.strftime("Time Taken To Save Animation: %H:%M:%S",
                                 time.gmtime(time.clock() - start)))
            print("animation saved...")

        pl.show()

class Convection2D(object):
    """
    Solver for Linear 2D Convection Equations:
    """
    pass

class Burgers1DInvisid(Convection1D):
    """
    Solver for Non Linear Inviscid Convection Equation: Usually known
    """

    def __init__(self, input_file):
        super(Burgers1DInvisid, self).__init__(input_file)

    def set_root(self):
        root = self.inputs.get_nodes('./solver/convection/burgersinviscid/one_dimensional')
        return root

    @property
    def window_title(self):
        return 'Burgers Inviscid equation 1D'

    def get_velocity(self, U, i, j):
        return U[i, j-1]

class Burgers2DInvisid(object):
    pass

class Burgers1DViscous(object):
    pass

class Burgers2DViscous(object):
    pass