from unittest import TestCase

from ..heatconduction import HeatConduction1D


class TestHeatConduction1D(TestCase):

    def setUp(self):
        self.heat1D_obj = HeatConduction1D('heatconduction/test/resources/input.xml')

    def test_solve(self):
        self.heat1D_obj.solve()

    def test_plot(self):
        pass
