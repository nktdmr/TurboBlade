from unittest import TestCase

from ..heatconduction import HeatConduction2D


class TestHeatConduction2D(TestCase):
    def setUp(self):
        self.heat_conduction = HeatConduction2D(input_file='heatconduction/test/resources/input.xml')

    def test_equation(self):
        self.heat_conduction.calc_temperature()