import numpy as np
import time
import pylab as pl

from src.cli.cli import print_progress_bar as ppb
from src.cli.xml_parser import XMLParser
from matplotlib import animation, cm
from mpl_toolkits.mplot3d import Axes3D
from ..solver import Solver1D
from ..solver import Solver2D


class HeatConduction1D(object):
    """
    This is a heat conduction class which solve by heat conduction equation
    i.e. dT/dt = alpha * d^2 T/ dx^2
    """

    def __init__(self, input_file):
        """

        Args:
            input_file:
        """
        self.inputs = XMLParser(input_file)
        self.root = self.inputs.get_nodes(
            './solver/heatconduction/one_dimensional')

    def solve(self):
        """
        Solve the 1D heat conduction equation
        Returns:

        """
        xn = self.inputs.get_value('./npx', self.root)
        total_time = self.inputs.get_value('./time', self.root)
        dt = self.inputs.get_value('./time_step', self.root)

        tn = int(total_time / dt)
        T = np.ndarray(shape=[xn, tn], dtype=float, order='c')

        start = time.clock()

        print ("Applying Boundary Conditions . . . . . . . . . . . . . . . .")

        initial_temp = self.inputs.get_value('./initial_bc', self.root)
        T.fill(initial_temp)

        heat_flux = self.inputs.get_value('./outer_bc', self.root)
        # noinspection PyCompatibility
        for j in xrange(0, tn):
            T[xn - 1, j] = heat_flux

        print ("\nBoundary Condition Applied")

        alpha = self.inputs.get_value('./alpha', self.root)
        length = self.inputs.get_value('./length', self.root)
        dx = length / (xn - 1)
        print ('\n')
        ppb(0, tn - 1, prefix='Progress:', suffix='Complete', length=75)

        # noinspection PyCompatibility
        for j in xrange(1, tn):
            # noinspection PyCompatibility
            for i in xrange(1, xn - 1):
                T[i, j] = T[i, j - 1] + (alpha * (dt / (dx * dx))) * (
                    T[i + 1, j - 1] - 2.0 * T[i, j - 1] + T[i - 1, j - 1])
            ppb(j, tn - 1, prefix='Progress:', suffix='Complete', fill="#")

        speed = self.inputs.get_value('./animate_speed', self.root)
        print(time.strftime("Time Taken: %H:%M:%S", time.gmtime(time.clock() -
                                                                start)))
        # print('Time Taken:  ' + str(time.clock() - start) + ' sec')
        self.animate_graph(length, xn, T, tn, initial_temp, heat_flux, speed)
        self.plot(T, tn, xn, length, total_time)

    @property
    def xlabel(self):
        return "Length of Material/Element in X-Direction in Meters"

    @property
    def ylabel(self):
        return "Temperature in deg celcius"

    @property
    def window_title(self):
        return "Heat Conduction 1D"

    def animate_graph(self, length, npts, T, tn, initial, final, speed):
        """
        This method animates the graph of Temperature vs Length with respect
        to time
        Args:
            length: Length of the material
            npts: Number of points in X direction - Equidistance
            T: Temperature as results
            tn: Number of time steps
            initial: Initial Temperature of the material
            final: Heat Flux added to the material as boundary condition
            speed: Speed of the animation

        Returns:

        """
        fig = pl.figure()
        fig.canvas.set_window_title('HeatConduction1D')
        ax = fig.add_subplot(111, autoscale_on=False, xlim=((0 - 0.1 * length),
                                                            length * 1.1),
                             ylim=(initial * 0.9, final * 1.1))
        ax.grid()
        dx = length / (npts - 1)
        x = np.arange(0, length, dx)

        line, = ax.plot([], [], 'o--')
        time_template = 'time = %.1fs'
        time_text = ax.text(0.05, 0.9, '', transform=ax.transAxes)

        def animate(i):
            y = T[:, i * speed]
            line.set_data(x, y)
            time_text.set_text(time_template % (i * speed))

            return line, time_text

        def init():
            line.set_data([], [])
            time_text.set_text('')
            return line, time_text

        # Init only required for blitting to give a clean slate.
        n_frames = int((tn - 1) / speed)
        ani = animation.FuncAnimation(fig, animate,
                                      frames=n_frames,
                                      interval=1,
                                      init_func=init,
                                      blit=True)
        ax.grid(linestyle='--')
        pl.xlabel(self.xlabel)
        pl.ylabel(self.ylabel)
        pl.title(self.window_title)

        anim_save = self.inputs.get_value('./save_animate', self.root)

        if anim_save:
            print("Saving Animation. This may Take a while.....\n")
            start = time.clock()
            ani.save('animate.mp4', writer=animation.FFMpegWriter(fps=100),
                     dpi=80)
            print (time.strftime("Time Taken To Save Animation: %H:%M:%S",
                                 time.gmtime(time.clock() - start)))
            print("animation saved...")

        pl.show()

    def plot(self, temp, _tn, _xn, length, tot_time):
        """
        Plot the contour of the results
        Args:
            temp: Temperature array
            _tn: size of the times
        Returns:
        """
        x, y = np.mgrid[0:length:(_xn * 1j), 0:tot_time:(_tn * 1j)]

        fig, axes = pl.subplots(1)
        fig.canvas.set_window_title(self.window_title)
        c1 = axes.contourf(x, y, temp, cmap=pl.get_cmap('rainbow'))
        pl.colorbar(c1, ax=axes, label="Temperature Deg C")
        pl.xlabel(self.xlabel)
        pl.ylabel('Time Passed in seconds')
        pl.title(self.window_title)
        fig.savefig('Heat1DPlot.png')
        pl.close(fig)
        pl.show()


class HeatConduction2D(object):
    """
    Heat Conduction solution for two dimension.
    """

    def __init__(self, input_file):
        self.inputs = XMLParser(input_file)
        self.root = self.inputs.get_nodes(
            './solver/heatconduction/two_dimensional')

    def calc_temperature(self):
        """
        Returns:
        """

        length = self.inputs.get_value('./geometry/length', top_node=self.root)
        height = self.inputs.get_value('./geometry/height', top_node=self.root)

        xn = self.inputs.get_value('./meshing/npts_x', top_node=self.root)
        yn = self.inputs.get_value('./meshing/npts_y', top_node=self.root)

        tn = self.inputs.get_value('./iteration/time', top_node=self.root)
        time_step = self.inputs.get_value('./iteration/time_step',
                                          top_node=self.root)

        tn /= time_step
        tn = int(tn)

        alpha = self.inputs.get_value('./boundary_condition/alpha',
                                      top_node=self.root)
        bc1 = self.inputs.get_value('./boundary_condition/initial_temp',
                                    top_node=self.root)
        left_wall = self.inputs.get_value('./boundary_condition/left_wall',
                                          top_node=self.root)
        right_wall = self.inputs.get_value('./boundary_condition/right_wall',
                                           top_node=self.root)
        upper_wall = self.inputs.get_value('./boundary_condition/upper_wall',
                                           top_node=self.root)
        lower_wall = self.inputs.get_value('./boundary_condition/lower_wall',
                                           top_node=self.root)

        T = np.ndarray(shape=(xn, yn, tn), dtype=float, order='c')

        T.fill(bc1)

        dx = length / xn
        dy = height / yn
        dt = time_step

        start = time.clock()

        print("Applying Boundary Conditions \n")

        ppb(0, tn - 1, prefix='Progress:', suffix='Complete', length=75)
        # noinspection PyCompatibility
        for t in xrange(0, tn):
            for j in xrange(0, yn):
                T[0, j, t] = left_wall
                T[xn - 1, j, t] = right_wall
            for i in xrange(0, xn):
                T[i, 0, t] = lower_wall
                T[i, yn - 1, t] = upper_wall
            ppb(t, tn - 1, prefix='Progress:', suffix='Complete', fill='#')

        print("BC: Applied!")

        # Keeping it outside loop as dt and dx are uniform
        sx = alpha * dt / (dx ** 2)
        sy = alpha * dt / (dy ** 2)

        print ('\n')
        print("Calculating Temperatures: \n")
        ppb(0, tn - 1, prefix='Progress:', suffix='Complete', length=75)

        for t in xrange(1, tn):
            for j in xrange(1, yn - 1):
                for i in xrange(1, xn - 1):
                    T[i, j, t] = T[i, j, t - 1] + \
                                 sx * (T[i, j + 1, t - 1] -
                                       2.0 * T[i, j, t - 1] +
                                       T[i, j - 1, t - 1]) + \
                                 sy * (T[i + 1, j, t - 1] -
                                       2.0 * T[i, j, t - 1] +
                                       T[i - 1, j, t - 1])
            ppb(t, tn - 1, prefix='Progress:', suffix='Complete', fill='#')

        # T.dump("Data")

        print(time.strftime("Time Taken: %H:%M:%S", time.gmtime(time.clock() -
                                                                start)))

        x, y = np.mgrid[0:length:(xn * 1j), 0:height:(yn * 1j)]

        self.animate_graph(T, x, y, tn, dt, lower_wall, left_wall,
                           upper_wall, right_wall)

    def animate_graph(self, T, x, y, tn, dt, lower_wall, left_wall,
                      upper_wall, right_wall):

        speed = self.inputs.get_value(
            './post_processing/animate/animate_speed', self.root)

        n_frames = (tn - 1) / speed

        # Set up plotting
        fig = pl.figure()
        fig.canvas.set_window_title('HeatConduction2D')
        ax = pl.axes()
        time_template = 'time = %.1fs'
        time_text = ax.text(0.05, 0.9, '', transform=ax.transAxes,
                            color='white')

        pl.xlabel('Length of Material/Element in X-Direction in Meters \n '
                  'Boundary Condition:  {} Deg C'.format(lower_wall))

        pl.ylabel('Length of Material/Element in Y-Direction in Meters \n '
                  'Boundary Condition: {} Deg C'.format(left_wall))
        pl.title("2D Heat Conduction: \n Boundary Condition: {}".format(
            upper_wall))

        # Animation function

        c_map = self.inputs.get_value('./post_processing/animate/color_map',
                                      self.root)

        def animate(i):
            ax.collections = []
            z = T[:, :, i * speed]
            cont = pl.contourf(x, y, z, cmap=pl.get_cmap(c_map))
            time_text.set_text(time_template % (i * dt * speed))
            ppb(i, n_frames - 1, prefix='Progress:', suffix='Complete',
                fill="#")
            return cont

        def init():
            time_text.set_text('')
            return

        anim = animation.FuncAnimation(fig, animate, frames=n_frames,
                                       interval=1, init_func=init)

        save_gif = self.inputs.get_value(
            './post_processing/animate/save_animate', self.root)
        pl.colorbar(animate(0), ax=ax, label="Temperature Deg C")
        pl.gca().set_aspect('equal')

        if save_gif:
            print ("saving animation! This may take a while.......\n")
            start = time.clock()
            anim_dpi = self.inputs.get_value('./post_processing/animate'
                                             '/animation_dpi', self.root)
            anim.save('animate_{}_{}_{}_{}.mp4'.format(left_wall, upper_wall,
                                                       right_wall, lower_wall),
                      writer=animation.FFMpegWriter(fps=50), dpi=anim_dpi)

            print(time.strftime("Time Taken: %H:%M:%S",
                                time.gmtime(time.clock() - start)))

            print ("animation saved:")

        pl.show()

    def save_animation(self):
        pass

    def apply_bc(self):
        pass
