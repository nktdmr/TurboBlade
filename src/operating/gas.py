"""
gas Properties
"""


class Gas_Type(object):

    def __init__(self, cp, cv):
        self.cp = cp
        self.cv = cv


class Gas(object):

    def __init__(self, gas):
        """

        Args:
            gas (Gas_Type): Gas Type like Dry Air, Air and Fuel Mixture

        Returns:
            object: 
        """
        self.cp = gas.cp
        self.cv = gas.cv
        self.R = gas.cp - gas.cv
        self.gamma = gas.cp/gas.cv

    @property
    def cp(self):
        return self._cp

    @cp.setter
    def cp(self, value):
        self._cp = value

    @cp.deleter
    def cp(self):
        del self._cp

    @property
    def cv(self):
        return self._cv

    @cv.setter
    def cv(self, value):
        self._cv = value

    @cv.deleter
    def cv(self):
        del self._cv

