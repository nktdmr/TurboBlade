import numpy as np


class Atmosphere(object):

    def __init__(self, ht):
        self.pressure, self.Temp, self.density = self.Flight_Condition(ht)

    @staticmethod
    def Flight_Condition(ht):
        ra = 6380.00 * 1000
        hg = 0.0
        ha = hg + ra
        g0 = 9.81
        g0 = g0 * pow((ra / ha), 2)
        t0 = 288.15
        r = 287.57
        slope = 0.0
        density = 0.0
        pressure = 0.0
        temperature = 0.0

        if ht <= 11000:
            slope = (216.0 - 288.15) / 11000
            temperature = (slope * (ht - 0.0)) + t0
            p1 = t0 * r * 1.233
            pressure = p1 * pow((temperature / t0), (g0 / slope * r))
            density = pressure / temperature * r
        elif 11000 < ht <= 20000:
            temperature = 216.0
            pressure = np.exp(-(g0 / r * temperature) * (ht - 11000))
            density = pressure / temperature * r
        elif 20000 < ht <= 31000:
            slope = (238.0 - 216.0) / (31000 - 20000)
            p1 = t0 * r * 1.233
            temperature = (slope * (ht - 20000)) + t0
            pressure = p1 * pow((temperature / t0), (g0 / slope * r))
            density = pressure / temperature * r
        elif ht == 31000:
            t0 = (slope * (31000.0 - 20000.0)) + t0
            temperature = t0
        elif 31000 < ht <= 48000:
            slope = (270.0 - 238.0) / (48000 - 32000)
            temperature = (slope * (ht - 31000)) + t0
            p1 = t0 * r * 1.233
            pressure = p1 * pow((temperature / t0), (g0 / slope * r))
            density = pressure / temperature * r
        elif ht == 48000:
            t0 = (slope * (48000 - 31000.0)) + t0
            temperature = t0
        elif 48000 < ht <= 53000:
            temperature = t0
            pressure = np.exp(-(g0 / r * temperature) * (ht - 11000))
            density = pressure / temperature * r

        return density, pressure, temperature
