

def maxSubstring(s):
    mx = []

    for i, ch in \
            (s):
        mx.append(ch)
        mx.append(''.join(maxSubstring(s[i+1:])))

    return mx

if __name__ == '__main__':
    print maxSubstring('banana')


