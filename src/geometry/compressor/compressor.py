from math import sqrt, pi, tan, atan2, cos, acos, ceil
import numpy as np
from ..annulus.annulus import Annulus


class Compressor_Data_Model(object):
    """
    Compressor Data Model using for Storing Compressor inputs
    May be we can eliminate it later
    """

    def __init__(self):
        pass


class Inlet_Diffuser(object):
    """
    Class for inlet diffuser of a compressor
    """
    pass


class Exit_Diffuser(object):
    """
    Class of exit diffuser of a compressor
    """
    pass


class Stage(object):
    """
    Stage of a compressor or Turbine
    """

    def __init__(self, rotor, stator, gap, **kwargs):
        """
        Constructor of Stage class, Stage consist of Rotor and Stator
        Args:
            rotor: Rotor of the stage
            stator: Stator of the Stage
            gap: Gap between stator and rotor
            **kwargs:
                temp_rise:
                bleed_flow:
                st_mfr:
        """
        self.rotor = rotor
        self.gap = gap
        self.stator = stator
        self.temp_rise = kwargs.get('temp_rise')
        self.bleed_flow = kwargs.get('bleed_flow')
        self.st_mfr = kwargs.get('st_mfr')
        self.calc_hub_to_tip_ratio = kwargs.get('h_t_ratio_flag')
        self.igv = kwargs.get('igv')
        self.n_sec = kwargs.get('n_sec')

    def calc_meanline(self):
        """

        Returns:

        """
        pass

    def write_meanline(self):
        """

        Returns:

        """
        pass

    def write_blade_angles(self):

        """

        Returns:

        """
        pass

    def calculate_params(self, design_law):
        """
        calculate root to tip parameters invoking design laws
        Args:
            design_law: Design laws to construct blade geometry available
                design laws 1. Free vortex law 2. constant reaction 3.
                exponential

        Returns:

        """
        for i in range(0, self.n_sec):
            # calculate every param
            pass

    def exit_mfr(self):
        """

        Returns:

        """
        pass

    def blockage(self):
        """

        Returns:

        """
        pass


class Rotor(object):
    """
    Class for Rotor
    """

    def __init__(self, i_radius, o_radius, **kwargs):
        """

        Args:
            i_radius (tuple): radius at mean blade height at inlet
            o_radius (tuple): radius at mean blade height at outlet
            kwargs:
                design_law: Design law used for creating blade
        """
        self.i_hub_rad = i_radius[0]
        self.i_tip_rad = i_radius[1]
        self.o_hub_rad = o_radius[0]
        self.o_tip_rad = o_radius[1]
        self.design_law = kwargs.get('design_law')
        self.spacing = kwargs.get('pitch')
        self.diff_factor = kwargs.get('diffusion_factor')
        self.thick_chord_ratio = kwargs.get('t_c_ratio')
        self.chord_height_ratio = kwargs.get('c_h_ratio')
        self.pitch_chord_ratio = kwargs.get('p_c_ratio')
        self.cp = kwargs.get('cp')
        self.work_done_factor = kwargs.get('wdf')
        self.temp_rise = kwargs.get('temp_rise')
        self.igv_angle = kwargs.get('igv_angle')
        self.avdr = kwargs.get('avdr')
        self.axial_velocity = kwargs.get('axial_velocity')

    def stator_width(self):
        """

        Returns:

        """
        self.blade_height_avg() * self.chord_height_ratio * cos(
            self.stagger_angle(self.i_tip_rad))

    def calc_diffusion_factor(self, r, v_max):
        """

        Args:
            r: radius at which diffusion factor needs to be calculated
            v_max: Maximum velocity calculated over an airfoil via potential
            panel method or mises calculation boundary displacement method

        Returns:

        """
        df = (v_max - self.v2(r)) / self.v1(r)
        return df

    def inlet_incidence(self, r, id):
        """
        Args:
            r (float):
            id (int):
        Returns:
            float:

        """
        return self.aia(id) - self.bia(r)

    def exit_deviation_angle(self, r):
        """

        Returns:

        """
        return self.aoa(r) - self.boa(r)

    def deflection(self, r):
        """

        Args:
            r:

        Returns:

        """
        return self.aia(r) - self.aoa(r)

    def calc_theta(self, r):
        """
        Args:
            r (float):
        Returns:
            float:

        """
        return self.exit_deviation_angle(r) * sqrt(self.solidity()) / self.m_rule(r)

    def m_rule(self, r, a=0.4):
        """

        Args:
            r (float):
            a (float): a is the distance of the point of maximum camber from
            the leading edge of teh blade.

        Returns:

        """
        return 0.2300 * (2.0 * a / self.chord(r)) ** 2 + 0.1 * self.aoa(r) / 50

    def chord(self, r):
        """

        Args:
            r (float):

        Returns:

        """
        return self.blade_height_avg()

    def stagger_angle(self, r):
        """

        Args:
            r: radius at which settings angle is calculated

        Returns:

        """
        return self.bia(r) - (self.calc_theta(r) / 2.0)

    def blade_height_out(self):
        """
        Returns:

        """
        return self.o_tip_rad - self.o_hub_rad

    def blade_height_in(self):
        """

        Returns:

        """
        return self.i_tip_rad - self.i_hub_rad

    def blade_height_avg(self):
        """
        average blade height

        Returns:
            float:

        """
        return (self.blade_height_out() + self.blade_height_in()) / 2.0

    def calc_chord(self, r):
        """

        Args:
            r:

        Returns:

        """
        pass

    def number_of_blades(self, r):
        """
        number stators in the compressor ring at this stage
        Args:
            r(float): solidity or spacing or pitch

        Returns:

        """
        pass

    def degree_of_reaction(self, r):
        """

        Returns:

        """
        pass

    def diffusion_factor(self, r):
        """

        Args:
            r:

        Returns:

        """
        df = 1 - (self.v2(r)/self.v1(r))
        df += self.dCW(r) * self.spacing / (self.chord(r) * 2.0 * self.v1(r))

        return df

    def calc_spacing(self, r, df):
        """
        Args:
            r:
            df:

        Returns:

        """
        d = df
        s = (d - 1 + self.v2(r)/self.v1(r)) * 2 * self.v1(r) * self.chord(r)
        s /= self.dCW(r)

        return s

    def dor(self, r):
        """
        short hand for degree of reaction
        Returns:

        """
        return self.degree_of_reaction(r)

    def pitch(self):
        """

        Returns:

        """
        return self.spacing

    def solidity(self):
        """

        Returns:

        """
        return self.pitch()

    def bia(self, r):
        """
        blade inlet angles
        Args:
            r:

        Returns:

        """
        return atan2(self.u(r) - self.cw1(r), self.ca1(r))

    def boa(self, r):
        """
        blade outlet angle
        Args:
            r(float):

        Returns:
            float:

        """
        return atan2(self.u(r) - self.cw2(r), self.ca2(r))

    def aia(self, r):
        """
        air inlet angle at radius r
        Args:
            r (float):

        Returns:
            float:

        """

        return atan2(self.cw1(r), self.ca1(r))

    def aoa(self, r):
        """
        air outlet angle at radius r
        Args:
            r (float):

        Returns:
            float:

        """
        return atan2(self.cw2(r), self.ca2(r))

    def u(self, r):
        """
        Blade speed

        Args:
            r:

        Returns:

        """
        return 0.0

    def ca1(self, r):
        """
        inlet axial velocity at radius r
        Args:
            r (float):

        Returns:
            float:

        """
        return self.axial_velocity

    def ca2(self, r):
        """
        outlet axial velocity at radius r
        Args:
            r (float):

        Returns:
            float:

        """
        return self.ca1(r) * self.avdr

    def c1(self, r):
        """
        inlet absolute velocity at radius r
        Args:
            r (float):

        Returns:
            float:

        """
        return self.cw1(r) + self.ca1(r)

    def c2(self, r):
        """
        outlet  absolute velocity att radius r
        Args:
            r (float):

        Returns:
            float:

        """
        pass

    def v1(self, r):
        """
        inlet relative velocity at radius r
        Args:
            r (float):

        Returns:
            float:
        """
        return self.ca1(r) + self.u(r) - self.cw1(r)

    def v2(self, r):
        """
        outlet relative velocity at radius r
        Args:
            r (float):
        Returns:
            float:
        """
        return self.v1(r) * cos(self.bia(r)) / cos(self.boa(r))

    def cw2(self, r):
        """
        outlet tangential component of velocity at radius r
        Args:
            r (float):

        Returns:
            float:

        """
        return self.calc_dCW(r) + self.cw1(r)

    def cw1(self, r):
        """
        Inlet tangential component of velocity at radius r
        Args:
            r (float):

        Returns:
            float:

        """

        # get section id based on the radius
        return tan(self.aia()) * self.ca1(r)

    def calc_dCW(self, r):
        """
        calculate delta CW based on the temperature rise across the rotor
        Args:
            r (float):

        Returns:

        """
        return self.cp * self.temp_rise / self.work_done_factor * self.u(r)

    def dCW(self, r):
        """

        Args:
            r (float):

        Returns:

        """
        return self.cw2(r) - self.cw1(r)


class Stator(Rotor):
    """
    Class for Stator
    """

    def __init__(self, i_radius, o_radius, nrt, **kwargs):
        """

        """
        super(Stator, self).__init__(i_radius, o_radius, **kwargs)
        self.blade_speed = nrt



class Gap(object):
    """
    Class for Rotor stator Gap This can be eliminated if Exact Rotor stator
        Distance is available
    """
    pass


class Stage_Gap(object):
    """
    Class for Stage Gap
    """

    def __init__(self):
        """

        """
        pass


class Compressor(object):
    """
    Preliminary Compressor Design,
    1. Which Accounts for Max RPM based on Material choice of Blade
    2. Max Tip Speeds , Limited to Transonic Speeds
    3. Axial Velocity is considered constant throughout compressor.
    4. Limited High Fluid Deflections in Rotor Blades. i.e. de Haller Number
    is >= 0.72 and not less than 0.72. V2/V1 >= 0.72
    5. Diffusion factor is also considered.
    """

    def __init__(self, **kwargs):
        """
        Compressor
        Args:
            kwargs: High level kwargs definition
            dir_rotation: Direction of rotation -1 for anti-clock wise and +1
                for clockwise
            n_stage: number of compressor stages
            rpm: Rotation per minute of compressor
            ht_ratio: hub to tip radius ratio
            tip_mach: Tip mach number for first stage
            temperature: inlet in_temperature to the compressor
            pressure: inlet in_pressure to the compressor
            density: inlet fluid in_density to the compressor
            ca: axial inlet velocity
            gas: gas used in compressor: default should be dry air
            core_mfr: inlet mass flow rate / core mass flow rate
            req_pr: required in_pressure ratio
            poly_eff: assumed polytropic efficiency of the compressor
            design_law: used design law to design blade section for 1st stage.
            de_hallers: exit relative velocity/ inlet relative velocity
            igv_angle: 0 is no igv is present and +ve angles in deg is IGV is
                present


        """
        self.direction_of_rotation = kwargs.get('dir_rotation')
        self.n_stage = kwargs.get('n_stage')
        self.rpm = kwargs.get('rpm')
        self.hup_tip_ratio = kwargs.get('ht_ratio')
        self.tip_mach = kwargs.get('tip_mach')
        self.in_temperature = kwargs.get('in_temperature')
        self.in_density = kwargs.get('in_density')
        self.in_pressure = kwargs.get('in_pressure')
        self.axial_vel = kwargs.get('ca')
        self.gas = kwargs.get('gas')
        self.mfr = kwargs.get('core_mfr')
        self.req_pr_ratio = kwargs.get('req_pr')
        self.polytropic_efficiency = kwargs.get('poly_eff')
        self.design_law = kwargs.get('design_law')
        self.de_hallers = kwargs.get('de_hallers')
        self.inlet_igv = kwargs.get('igv_angle')
        self.annulus = Annulus()
        self.inlet_diffuser = Inlet_Diffuser()
        self.exit_diffuser = Exit_Diffuser()

    @staticmethod
    def stages(n_stage):
        """
        Return the numpy list of all the stages
        Args:
            n_stage (int): Number of stages

        Returns:

        """
        st_list = np.ndarray(shape=[n_stage], dtype=Stage, order='c')
        for st in range(0, n_stage):
            rotor = Rotor()
            stator = Stator()
            gap = Gap()

            st_list[st] = Stage(rotor, stator, gap)

        return st_list

    def calculate_stages(self, n_stage):
        """

        Args:
            n_stage:

        Returns:

        """
        pass

    def inlet_radius(self, tip_mach):
        """
        Calculate Hub to Tip ratio when max tip mach number is supplied
        usually compressor does not exceed transonic speeds

        Args:
            tip_mach(float): Max Mach number at Blade tip compressor design

        Returns:
            float: tip radius, hub radius

        """
        velocity = tip_mach * sqrt(self.gas.gamma * self.gas.R *
                                   self.in_temperature)

        angular_velocity = self.angular_velocity(self.rpm)

        tip_radius = velocity / angular_velocity

        hub_radius = sqrt(tip_radius ** 2.0 - (self.mfr / (self.in_density *
                                                           velocity * pi)))

        return tip_radius, hub_radius

    def exit_radius(self, rm):
        """
        Exit Hub and tip radius
        Args:
            rm (float):

        Returns:

        """
        pass

    def temperature_rise(self, pr_ratio, in_temp):
        """
        Calculates the Temperature rise across the compressor based on the
        required in_pressure rise.
        Assuming gas does not change its specific heat coefficients across the
        compressor we can assume a constant gas constant across the
        compressor and thus gamma value also does'nt change
        can use constant
        Args:
            pr_ratio:
            in_temp:

        Returns:

        """
        ratio = self.gas.gamma / (self.gas.gamma - 1)
        return in_temp * pr_ratio ** ratio

    def inlet_bia(self, r):
        """

        Args:
            r (float):

        Returns:

        """
        bia = atan2(self.blade_speed(r, self.rpm), self.axial_vel)
        return bia

    def boa(self, r):
        """
        Blade outlet angle
        Args:
            r (float): radius at which outlet blade angle needs to be calculated

        Returns:

        """
        return acos(self.axial_vel / self.outlet_relative_vel(r))

    def first_stage_temperature_rise(self, r):
        """
        First Stage temperature rise at mean radius
        Args:
            r (float):

        Returns:
            float:

        """
        temp_rise = self.polytropic_efficiency * self.blade_speed(r, self.rpm)
        temp_rise *= self.axial_vel * tan(self.inlet_bia(r) - tan(self.boa(r)))
        temp_rise /= self.gas.cp
        return temp_rise

    def number_of_stages(self, r, extra=1):
        """
        Number of stages calculated based on average temperature rise by
        first stage
        Args:
            r (float): Mean Radius
            extra (int): Extra Stages needs to considered

        Returns:
            int: number of stages required for doing the required temperature
            rise

        """
        return int(ceil(self.temperature_rise(self.req_pr_ratio,
                                              self.in_temperature)
                        / self.first_stage_temperature_rise(r)) + int(extra))

    def inlet_hub_to_tip_ratio(self, tip_mach):
        """
        inlet hub to tip ratio
        Args:
            tip_mach (float):

        Returns:
            float:

        """
        tip_rad, hub_rad = self.inlet_radius(tip_mach)
        return tip_rad / hub_rad

    def inlet_mean_radius(self, tip_mach):
        """
        inlet mean radius
        Args:
            tip_mach (float):

        Returns:
            float:

        """
        tip_rad, hub_rad = self.inlet_radius(tip_mach)

        return (tip_rad + hub_rad) / 2

    @staticmethod
    def angular_velocity(rpm):
        """
        angular velocity
        Args:
            rpm (float): Revolution per minute

        Returns:
            float:

        """
        return 2.0 * pi * rpm / 60

    @staticmethod
    def blade_speed(r, rpm):
        """
        blade speed at radius r when compressor is rotating rpm revs per minute
        Args:
            r (float):
            rpm (float):

        Returns:
            float:

        """

        return 2.0 * pi * r * rpm

    def inlet_relative_vel(self, r):
        """
        inlet relative velocity at radius r
        Args:
            r (float):

        Returns:
            float:

        """
        return self.axial_vel / cos(self.inlet_bia(r))

    def outlet_relative_vel(self, r):
        """
        Relative outlet velocity at the exit of first stage
        Args:
            r (float): radius

        Returns:
            float:

        """
        return self.inlet_relative_vel(r) * self.de_hallers

    def blade_rows(self, comp):
        """
        Valid Blade Rows for a given number of stages
        Args:
            comp (str):
        Returns:
            list:

        """
        bladerows = []
        for i in range(0, self.n_stage):
            bladerows.extend(['{}{}S'.format(comp, str(i + 1).zfill(2)),
                              '{}{}R'.format(comp, str(i + 1).zfill(2))])
        return bladerows

    @property
    def compressor_length(self):
        """
        Total compressor length
        Returns:

        """
        return self.compressor_length

    @compressor_length.setter
    def compressor_length(self, value):
        """
        setter for compressor length based on user input
        Args:
            value:

        Returns:

        """
        if value == 0.0:
            self._compressor_length = self.calculate_compressor_length()
        else:
            self._compressor_length = value

    def calculate_compressor_length(self):
        """
        Compressor Length Correlations based on blade height
        Returns:

        """
        return 0.0

    def calc_blade_chord(self, r):
        """
        Calculate blade cord at radius r
        Args:
            r (float):
        Returns:
            float:

        """
        return

    def calc_rotor_stator_spacing(self, r):
        """
        calculate rotor stator spacing based on blade height correlations
        Args:
            r (float):
        Returns:
            float:

        """
        return


class LP_Compressor(Compressor):
    pass


class Fan(LP_Compressor):
    pass


class IP_Compressor(Compressor):
    pass


class HP_Compressor(Compressor):
    pass
