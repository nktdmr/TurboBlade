from point import point2D, point3D
import numpy as np


class line2D(object):
    """
    Class Line2D
        Uses two points two create the object of this class
    """
    def __init__(self, start=point2D(), end=point2D(1.0, 1.0)):
        """
        Constructor of Line2D
        :param start: Start 2D Point
        :param end: End 2D Point
        """
        self.start = start
        self.end = end

    def length(self):
        """
        Length of the line in supplied units
        :return: Length of the line using distance formula
        """
        return np.sqrt((self.start.x - self.end.x) ** 2 + (self.start.y - self.end.y) ** 2)


class line3D(line2D):
    def __init__(self, start=point3D(), end=point3D(1.0, 1.0, 1.0)):
        """
        Constructor of Line3D
        :param start: Start 3D Point
        :param end: End 3D Point
        """
        #super(line3D, self).__init__(start, end)
        super(line3D, self).__init__(start, end)
        self.start = start
        self.end = end

    def length(self):
        """
        Length of the line in supplied Units
        :return: Length of the line using distance formula
        """
        return np.sqrt(
            (self.start.x - self.end.x) ** 2 + (self.start.y - self.end.y) ** 2 + (self.start.z - self.end.z) ** 2)
