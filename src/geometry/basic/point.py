"""

"""


class point2D(object):

    def __init__(self, x=0.0, y=0.0):
        """
        Constructor of Point2D
        :param x: X Value
        :param y: y Value
        """
        self.x = x
        self.y = y

    def __add__(self, other):
        """
        Adding the two points, As a vector, Points are considered as constructor
        :param other: This is the other point which needs to be added
        :return: Vector C = Vector A + Vector B
        """
        return point2D(self.x + other.x, self.y + other.y)

    def __mul__(self, scalar=1.0):
        """
        multiplication of a point/vector with a scalar
        :type scalar: Float
        """
        return point2D(self.x * scalar, self.y * scalar)

    def __str__(self):
        return '(' + str(self.x) + ', ' + str(self.y) + ')'

    def shift(self):
        """
        Shift of point to another location
        :return:None
        """
        pass


class point3D(point2D):
    """
    Class for point3D
    """

    def __init__(self, x=0.0, y=0.0, z=0.0):
        # type: (float, float, float) -> point3D
        """
        Constructor of Point 3D
        :param x: X value
        :param y: Y Value
        :param z: Z Value
        """
        super(point3D, self).__init__(x, y)
        self.z = z

    def __add__(self, other):
        """
        Addition of a 3D Vector point, similar to point 2D
        :param other:
        :return: return new 3D point of Vector
        """
        return point3D(self.x + other.x, self.y + other.y, self.z + other.z)

    def __mul__(self, scalar=1.0):
        """
        Multiplication of vector/point with a scalar
        :param scalar:
        :return:
        """
        return point3D(self.x * scalar, self.y * scalar, self.z * scalar)

    def dot(self, other):
        """

        :param other:
        :return:
        """
