"""
A python file for utilities for creating gas turbine geometry
"""
from math import sqrt


def speed_of_sound(gamma, R, T):
    """
    Speed of sound
    Args:
        gamma:
        R:
        T:

    Returns:

    """
    return sqrt(gamma * R * T)


def mach(free_stream_vel, sound_speed):
    """
    Mach Number : Ratio of Free stream velocity and Speed of Sound
    Args:
        free_stream_vel:
        sound_speed:

    Returns:

    """
    return free_stream_vel / sound_speed


def tT(t, gama, mach_no):
    """
    Total temperature
    Args:
        t: inlet static temperature
        gama: cp/cv = ratio of specific heats at constant pressure and volume
        mach_no:

    Returns:

    """

    return t * (1 + 0.5 * (gama - 1) * mach_no ** 2)


def tP(p, gama, mach_no):
    """
    Total pressure
    Args:
        p:
        gama:
        mach_no:

    Returns:

    """
    ratio = gama / (gama - 1)
    return p * (1 + 0.5 * (gama - 1) * mach_no ** 2) ** ratio
