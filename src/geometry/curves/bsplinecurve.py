import matplotlib.pyplot as plt
import numpy as np

from src.geometry.basic.point import point3D


class Bspline(object):
    """
    Class for BSpline implementation
    """

    def __init__(self, polyline=None, order=4, npt=100, udist=None):
        """
        Constructor for BSpline
        :param polyline: Polyline for the spline
        :param order: order of the spline
        :param npt: number of points
        :param udist: point distribution
        """
        self.npt = npt
        self.order = order
        assert isinstance(polyline, list), 'Argument of wrong type!'
        self.polyline = polyline
        if udist is None:
            self.udist = self.u_dist()
        else:
            self.udist = udist
        self.knots = [0, 0, 0, 0, 1, 1.5, 2, 3, 3, 3, 3]  # self.get_knots()
        self.curve = self.b_spline

    def get_knots(self):
        """
        :return:
        """
        knots = np.ndarray(shape=[len(self.polyline) + self.order + 1],
                                  dtype=float, order='C')
        m = len(knots)
        for i in range(0, m):
            if i < 4:
                knots[i] = 0.0
            elif i >= m - 4:
                knots[i] = 1.0
            else:
                knots[i] = float(i - 3) / (m - 7)
        return knots

    # TODO: Implent the u distribution based on alpha and beta distribution
    def u_dist(self):
        """
        :return: Uniformly distributed points
        """
        u = []
        for i in range(0, self.npt):
            u.append(float(i*3) / (self.npt - 1))

        return u

    @property
    def b_spline(self):
        """

        :return: Spline
        """
        curve = []
        for k in range(0, len(self.udist)):
            curve.append(self.get_cord(self.udist[k]))

        return curve

    def get_cord(self, u):
        """
        :param u:
        :param p:
        :return:
        """
        cord = point3D()
            # cord += self.polyline[k] * self.basis_function(u)[k]
        for k in range(0, len(self.knots)):
            cord = self.deBoor(k, u)

        return cord

    def deBoor(self, k, u):
        t = self.knots
        c = self.polyline
        p = self.order
        """
        Evaluates S(x).

        Args
        ----
        k: index of knot interval that contains x
        u: position
        t: array of knot positions, needs to be padded as described above
        c: array of control points
        p: degree of B-spline
        """
        # print
        d = [c[j + k - p] for j in range(0, p + 1)]
        print(c[j + k - p])
        # print d
        for r in range(1, p + 1):
            for j in range(p, r - 1, -1):
                print (t[j + 1 + k - r] - t[j + k - p])
                alpha = (u - t[j + k - p]) / (t[j + 1 + k - r] - t[j + k - p])
                d[j] = d[j - 1] * (1.0 - alpha) + d[j] * alpha
        print(d[p])
        return d[p]

    def basis_function(self, u):
        print(u)
        npts = len(self.polyline)
        knots = self.knots
        print(knots)
        p = self.order
        coef_n = [0] * npts
        if u == knots[0] or u == knots[len(knots) - 1]:
            coef_n[0] = 1.0
            coef_n[npts - 1] = 1.0
            return coef_n
        else:
            for k in range(0, npts - 1):
                print(k)
                if knots[k] < u <= knots[k + 1]:
                    coef_n[k] = 1.0
                    for d in range(1, p + 1):
                        coef_n[k - d] = ((knots[k + 1] - u) / (knots[k + 1] - knots[k - d + 1])) * coef_n[k - d + 1]
                        for j in (k - d + 1, k):
                            coef_n[j] = ((u - knots[j]) / (knots[j + d] - knots[j])) * coef_n[j] + \
                                        ((knots[j + d + 1] - u) / (knots[j + d + 1] - knots[j + 1])) * coef_n[j + 1]
                        coef_n[k] = (u - knots[k]) / (knots[k + d] - knots[k]) * coef_n[k]
        print(coef_n)
        return coef_n

    # TODO: Implement the Subdivision algorithms
    def sub_division(self, u):
        """
        :param u:
        :return:
        """
        return u


if __name__ == '__main__':
    x1 = [-4.0, 2.0, 6.0, 10.0, 14.0, 18.0]

    y1 = [-2.0, 2.0, -2.0, 2.0, -1.0, 2.0]
    p1 = point3D(-4.0, -2.0, 0.0)
    p2 = point3D(2.0, 2.0, 0.0)
    p3 = point3D(6.0, -2.0, 0.0)
    p4 = point3D(10.0, 2.0, 0.0)
    p5 = point3D(14.0, -1.0, 0.0)
    p6 = point3D(18.0, 2.0, 0.0)

    pts = [p1, p2, p3, p4, p5, p6]
    bc = Bspline(pts)
    x = []
    y = []

    for i in range(0, 100):
        # print str(bc.curve[i].x) + str(bc.curve[i].y) + str(bc.curve[i].z)
        x.append(bc.curve[i].x)
        y.append(bc.curve[i].y)

    print(x)
    print(y)

    plt.plot(x, y, '-o')
    plt.plot(x1, y1)
    plt.show()
