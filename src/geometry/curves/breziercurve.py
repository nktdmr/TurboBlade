import numpy as np
from src.geometry.basic.point import point3D
from math import factorial
import matplotlib.pyplot as plt


class Bezier(object):
    """
    Class Bezier, I can replace this class by just inheriting bspline class
    and  setting degree to Npt - 1
    """

    def __init__(self, polyline=None, npt=100, alpha=0.0, beta=0.0):
        """
        Constructor for Bezier Curve
        :param polyline: The Control Points which define Bezier curve
        :param npt: number of points
        :param alpha: Alpha coefficient for clustering in beta distribution
        :param beta:  Beta coefficient for clustering in beta distribution
        """
        self.alpha = alpha
        self.beta = beta
        if polyline is None:
            polyline = [point3D(), point3D(0.5, 0.6), point3D(1.6, 0.7),
                        point3D(2.0)]
        self.npt = npt
        self.polyline = polyline
        self.len = len(polyline)
        self.curve = np.ndarray(shape=[npt], dtype=point3D, order='C')

    def __add__(self, other, continuity):
        """

        :param other:
        :return:
        """
        return 'add functionality has not been implemented'

    def calculate_bezier(self):
        """

        :return:
        """
        for j in range(0, self.npt):
            u = float(j) / (self.npt - 1)
            self.curve[j] = self.get_cord(u)
        return

    def basic_function(self, k, u):
        """
        :param k: index of control point
        :param u: value of x position in the curve in percentatge(0 to 1)
        :return:  Return Basis function
        """
        n = self.len - 1
        bf = factorial(n) / (factorial(k) * factorial(n - k)) * \
             ((u ** k) * (1 - u) ** (n - k))
        return bf

    def get_cord(self, u=0.0):
        """
        This function will return the x,y,z coordinates when supplied the
        normalized location in range of 0 to 1
        :param u:Position in curve in range of 0 to 1
        :return: Coordinates
        """
        coord = point3D()
        for k in range(0, self.len):
            bf = self.basic_function(k, u)
            coord = coord + (self.polyline[k] * bf)
        return coord

    def bezier_derivative(self, d):
        """
        The Derivative of bezier curve
        :param d:
        :return:
        """
        pass

    def derivative_point(self, n):
        """
        Derivative of point d
        :param n: n = nth derivative
        :return: return the nth difference point
        """
        pass

    def move_control_point(self, index, v):
        """
        Move a control point with a vector v
        :param index: index of the Point to be moved
        :param v: vector v to which point needs to be moved
        :return: Modified curve, Array of point3D
        """
        new_curve = np.ndarray(shape=[self.npt], dtype=point3D, order='C')
        for j in range(0, self.npt):
            u = float(j) / (self.npt - 1)
            new_curve[j] = self.get_cord(u) + v * self.basic_function(index, u)

        return new_curve

    def sub_division(self, u):
        """
        Sub dividing the bezier curve
        :param u: the location of curve at which the curve needs to be divided at
        :return: Two array of polyline which produces two curves which are c1
        continuous at joining
        """
        new_pts = np.ndarray(shape=[2 * self.npt], dtype=point3D, order='C')
        for pt in range(1, len(self.polyline) - 1):
            new_pts[i + 1] = self.polyline[i] + self.polyline[i] * u

        return

    def degree_of_elevation(self, n):
        """
        Degree of elevation algorithm for calculating new set of points which
        returns same bezier curve
        :param n: Degree of elevation
        :return: return new polyline
        """
        return


if __name__ == '__main__':
    x1 = [-4.0, 2.0, 6.0, 10.0, 14.0, 18.0]

    y1 = [-2.0, 2.0, -2.0, 2.0, -1.0, 2.0]
    p1 = point3D(-4.0, -2.0, 0.0)
    p2 = point3D(2.0, 2.0, 0.0)
    p3 = point3D(6.0, -2.0, 0.0)
    p4 = point3D(10.0, 2.0, 0.0)
    p5 = point3D(14.0, -1.0, 0.0)
    p6 = point3D(18.0, 2.0, 0.0)

    pts = [p1, p2, p3, p4, p5, p6]
    bc = Bezier(pts)
    bc.calculate_bezier()
    x = []
    y = []

    for i in range(0, 100):
        # print str(bc.curve[i].x) + str(bc.curve[i].y) + str(bc.curve[i].z)
        x.append(bc.curve[i].x)
        y.append(bc.curve[i].y)

    plt.plot(x, y, '-o')
    plt.plot(x1, y1)
    plt.show()
