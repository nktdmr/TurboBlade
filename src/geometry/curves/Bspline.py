# coding=utf-8
from __future__ import print_function
import numpy as np
import scipy.interpolate as si
import pprint


class bspline(object):
    """
    B-Spline Class
    """
    def __init__(self, cv, degree=3, periodic=False, n=100, alpha=None, beta=None, der=0):
        """
        :param cv: Array ov control vertices
        :param degree: Curve degree
        :param periodic: True - Curve is closed  False - Curve is open
        :param n: Number of samples to return
        :param alpha: Alpha Clustering Coefficient
        :param beta: Beta Clustering Coefficient
        :param der: derivative of spline
        """
        self.der = der
        self.cv = cv
        self.beta = beta
        self.alpha = alpha
        self.cp = len(cv)
        self.n = n
        self.degree = degree
        self.periodic=periodic
        self.curve = self.spline

    def __str__(self):
        """
        OverRides the Object Print statement, Object Print Memory
        :return: None
        """

        pp = pprint.PrettyPrinter(indent=4)
        pp.pprint(self.curve)
        return super(bspline, self).__str__()

    def udist(self):
        u = np.linspace(self.periodic, (self.cp - self.degree), self.n)
        return u

    def alphabeta(self, alpha, beta):
        u = np.random.beta(alpha, beta)
        print(u)

    @property
    def spline(self):
        # If periodic, extend the point array by count+degree+1
        self.cv = np.asarray(self.cv)
        if self.periodic:
            factor, fraction = divmod(self.cp + self.degree + 1, self.cp)
            self.cv = np.concatenate((self.cv,) * factor + (self.cv[:fraction],))
            self.cp = len(self.cv)
            self.degree = np.clip(self.degree, 1, self.degree)

        # If opened, prevent degree from exceeding count-1
        else:
            self.degree = np.clip(self.degree, 1, self.cp - 1)

        # Calculate knot vectors
        if self.periodic:
            kv = np.arange(0 - self.degree, self.cp + self.degree + self.degree - 1, dtype='int')
        else:
            kv = np.array([0] * self.degree + range(self.cp - self.degree + 1) +
                          [self.cp - self.degree] * self.degree, dtype='int')

        # Calculate query range
        u = np.linspace(self.periodic, (self.cp - self.degree), self.n)

        # Calculate result
        arrange = np.arange(len(u))
        points = np.zeros((len(u), cv.shape[1]))
        for i in xrange(cv.shape[1]):
            points[arrange, i] = si.splev(u, (kv, self.cv[:, i], self.degree), der=self.der)

        return points


import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

colors = ('b', 'g', 'r', 'c', 'm', 'y', 'k')
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')

cv = np.array([[50., 25., 20.0],
               [59., 12., 21.0],
               [50., 10., 10.0],
               [57., 2., 06.0],
               [40., 4., 07.0],
               [40., 14., 08.0],
               [30., 15.0, 27.0],
               [40.0, 25.2, 69.0],
               [35., 36., 45.0],
               [36.0, 14.0, 10.0],
               [50., 10., 07.]])

# plt.plot(cv[:,0],cv[:,1],'o-', label='Control Points')
ax.plot(cv[:, 0], cv[:, 1], cv[:, 2], '-', label='Control Points')

for d in range(1, 4):
    bs = bspline(cv, degree=d, periodic=True)
    print (bs)
    p = bs.spline
    x, y, z = p.T
    # plt.plot(x,y,'k-',label='Degree %s'%d,color=colors[d%len(colors)])
    ax.plot(x, y, z, '-o', label='Degree %s' % d)

plt.minorticks_on()
plt.legend()
plt.xlabel('x')
plt.ylabel('y')
# plt.xlim(35, 70)
# plt.ylim(0, 30)
plt.gca().set_aspect('auto')
plt.show()
