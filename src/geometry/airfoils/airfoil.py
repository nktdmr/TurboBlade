class Airfoil(object):
    """
    Class for Airfoil
    """


    def __init__(self, title='Airfoil'):
        self.title = title

    def leading_edge(self):

        self.a = 10
        return [0.0], [0.0]

    def trailing_edge(self):
        print self.a
        return [self.chord], [0.0]

    def pressure_side(self):
        return [0.0], [0.0]

    def suction_side(self):
        return [0.0], [0.0]

    @property
    def npt(self):
        return 50

    @property
    def chord(self):
        return 1


a = Airfoil('1111')
