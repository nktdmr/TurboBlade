from src.geometry.airfoils.airfoil import Airfoil

class NACA(Airfoil):
    """
    Generic NACA class which store all the coordinates
    LE->SS->TE->PS
    """

    def __init__(self, naca):
        """
        Constructor for Generic NACA Airfoil
        :param naca: naca number
        """
        self.naca = naca
        self.cords = self.coordinate
        super(NACA, self).__init__('NACA-' + naca)

    @staticmethod
    def thickness(xc, t):
        """
        Return Thickness value Abs
        :param xc: x / chord ratio
        :param t: respective thickness at x/c
        :return: Thickness at x
        """
        a0 = 0.2969
        a1 = 0.126
        a2 = 0.3516
        a3 = 0.2843
        a4 = 0.1015
        yt = 5 * t * ((a0 * (xc ** 0.5)) - (a1 * xc) -
                      (a2 * (xc ** 2)) + (a3 * (xc ** 3)) - (a4 * (xc ** 4)))
        return yt

    @property
    def coordinate(self):
        """
        append all coordinate in clockwise direction
        :return:
        """
        ux = self.pressure_side()[0]
        lex = self.leading_edge()[0]
        x_cord_u = ux + lex
        uy = self.pressure_side()[1]
        ley = self.leading_edge()[1]
        y_cord_u = uy + ley
        lx = self.suction_side()[0]
        # tex = self.trailing_edge()[0]
        x_cord_l = lx
        x_cord_l.reverse()
        ly = self.suction_side()[1]
        # tey = self.trailing_edge()[1]
        y_cord_l = ly
        y_cord_l.reverse()
        x_coord = x_cord_u + x_cord_l
        y_coord = y_cord_u + y_cord_l

        return x_coord, y_coord

    def write_csv(self):
        """

        :return:
        """
        f = open('naca.csv', 'w')
        x = self.cords[0]
        y = self.cords[1]
        for i in range(0, len(self.cords[0])):
            s = str(x[i]) + ',  ' + str(y[i]) + ',  ' + str(0.0) + '\n'
            f.write(s)

