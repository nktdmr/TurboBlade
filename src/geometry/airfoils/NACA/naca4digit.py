from naca import NACA
import numpy as np
int

class NACA4Digit(NACA):

    """
    Class for NACA 4 Digit Airfoils
    p-> position->first digit
    m-> camber value->second digit
    t-> thickness->third digit
    """

    def __init__(self, naca='4415'):
        """
        Constructor of NACA 4 Digit Airfoil
        :type naca: NACA number
        :rtype: none
        :param naca: NACA number which is passed to super class
        """
        super(NACA4Digit, self).__init__(naca)

    @property
    def npt(self):
        """
        Number of Points
        :return:
        """
        return 100

    @property
    def m(self):
        """
        Camber Value
        :return: Camber value 100th of the first digit
        """
        m = self.naca[0]
        m = float(m)
        m /= 100.0
        return m

    @property
    def p(self):
        """
        Position
        :return: Position value 10th of second digit
        """
        p = self.naca[1]
        p = float(p)
        p /= 10.0
        return p

    @property
    def t(self):
        """
        Thickness value relative
        :return:MAX Thickness of the airfoil at 30% of chord from LE. it is 100th of the last two digit of NACA Number
        """
        t = self.naca[-2:]
        t = float(t)
        t = t / 100.0 * self.chord
        return t

    def pressure_side(self):
        """
        Coordinates of Lower Side
        :return: Coordinates of Lower side i.e Pressure side
        """
        x_cord = []
        y_cord = []
        c = self.chord
        t = self.t
        i = 0
        while i < self.npt:
            x = self.x_pos(i)
            xc = x / c
            yt = self.thickness(xc, t)
            yc = self.y_camber(xc)[0]
            theta = self.y_camber(xc)[1]
            x_cord.append(x + yt * np.sin(theta))
            y_cord.append(yc - yt * np.cos(theta))
            i = i + 1
        lower_side = (x_cord, y_cord)
        return lower_side

    def suction_side(self):
        """
        Coordinates of Upper Side
        :return: returns Coordinates of Upper Side that is Suction side
        """
        x_cord = []
        y_cord = []
        c = self.chord
        npt = self.npt
        t = self.t
        i = 0
        while i < npt:
            x = self.x_pos(i)
            xc = x / c
            yt = self.thickness(xc, t)
            yc = self.y_camber(xc)[0]
            theta = self.y_camber(xc)[1]
            x_cord.append(x - yt * np.sin(theta))
            y_cord.append(yc + yt * np.cos(theta))
            i = i + 1
        upper_side = (x_cord, y_cord)
        return upper_side

    def x_pos(self, i):
        """
        Position of x coordinate
        :param i: index of airfoil coordinate
        :return: Return absolute x value of airfoil along the chord
        """
        c = self.chord
        npt = self.npt
        x = c * 0.5 * (np.cos(i * (np.pi / npt)) + 1)
        return x

    def y_camber(self, xc):
        """
        y value of camber line
        :param xc:x/chord ratio
        :return: return value of camber at x/c
        """
        p = self.p
        m = self.m
        c = self.chord

        if xc <= p:
            yc = m * c / p ** 2 * (2 * p * xc - xc ** 2)
            dyc_dx = 2 * m / (p ** 2) * (p - xc)
            theta = np.arctan(dyc_dx)
        else:
            yc = m * c / (1 - p) ** 2 * \
                 ((1 - (2 * p) + (2 * p * xc)) - xc ** 2)
            dyc_dx = 2 * m / (1 - p) ** 2 * (p - xc)
            theta = np.arctan(dyc_dx)
        return yc, theta

#a = NACA4Digit('4415')

