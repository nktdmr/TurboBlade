import numpy as np
from naca import NACA


class NACA6digitAirfoil(NACA):
    def __init__(self, digit, naca):
        super(NACA6digitAirfoil, self).__init__(naca)
        self.naca_digit = digit

    def profile(self):
        """
        Profile BAsic calculation for Naca Airfoil 6 digit airfoil
        :return:
        """
        dradian = np.pi / 180.0
        inump = self.npt
        dXX = self.naca_digit[-2:]
        dSD = self.naca_digit[0]
        dMP = self.naca_digit[1]
        dRangeCli = self.naca_digit[2]
        dcli = self.naca_digit[3]
        chord = self.chord
        dcli = dcli / 10
        dXX = dXX / 100 * chord
        dx = chord
        da = 0.0
        for i in range(0, inump):
            dx = chord * 0.5 * (np.cos(i * dradian * (360.0 / inump)) + 1)
            dyt = self.thickness(dx / chord, dXX)
            if da == 1.0:
                if dx == chord:
                    dyc_c = 0.0
                    dslope = 0.0
                    dtheta = np.arctan(dslope)
                else:
                    dyc_c = -dcli * chord / 4 * np.pi * (
                        (1 - (dx / chord)) * np.log(1 - (dx / chord)) + (dx / chord) * np.log(dx / chord))
                    dslope = dcli / 4 * np.pi * ((np.log(1 - (dx / chord))) - np.log(dx / chord))
                    dtheta = np.arctan(dslope)
            elif da < 1.0:
                dg = -1 / (1 - da) * ((pow(da, 2) * (0.5 * np.log(da) - 0.25)) + 0.25)
                dh = ((1 - da) * ((0.5 * np.log(1 - da)) - 0.25)) + dg
                dc1 = 0.5 * pow((da - (dx / chord)), 2) * np.log(np.abs(da - (dx / chord)))
                dc2 = 0.5 * pow((1 - (dx / chord)), 2) * (1 - (dx / chord))
                dc3 = 0.25 * (pow((1 - (dx / chord)), 2))
                dc4 = 0.25 * (pow((da - (dx / chord)), 2))
                dc5 = (dx / chord) * np.log(dx / chord)
                dyc_c = (chord * dcli / (2 * np.pi)) * (
                    (1 / (1 - da)) * (dc1 - dc2 + dc3 + dc4) - dc5 + dg - dh * (dx / chord))
                dslope = (dcli / 2 * np.pi * (1 + da)) * (
                    ((1 / (1 - da)) * (((1 - (dx / chord)) * np.log(1 - (dx / chord))) -
                                       ((da - (dx / chord)) * np.log(da - (dx / chord))))) -
                    (np.log(dx / chord)) - 1 - dh)
                dtheta = np.arctan(dslope)
