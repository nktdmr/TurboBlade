from naca import NACA
import numpy as np
from src.cli.cli import *


class NACA5DigitAirfoil(NACA):
    def __init__(self, digit):
        super(NACA5DigitAirfoil, self).__init__(digit)
        self.naca_digit = digit
        pass

    @staticmethod
    def camber(xf):
        m_old = 1
        m_new = xf / (1 - np.sqrt(m_old / 3))
        if m_new > m_old:
            m_new = pow((1 - (xf / m_old)), 2) * 3
        a = 0
        while a != m_new:
            a = m_new
            m_new = xf / (1 - np.sqrt(m_new / 3))
        return a

    def profile(self):
        radian = np.pi / 180.0
        if len(self.naca_digit) is not 5:
            print_fail("Invalid NACA 5 Digit Profile")
            return 0
        nump = self.npt
        l = self.naca_digit[0]
        p = self.naca_digit[1]
        q1 = self.naca_digit[2]
        x_x = self.naca_digit[-2:]
        cli = 3.0 / 20.0 * l

        if p > 8:
            print_fail(
                "Invalid profile for NACA 5 Digit Number."
                "Value of Xf is Diverging.The value of Cm/4 is behind xf")
            return 0
        elif l != 2.0:
            print_fail(
                "Invalid profile for NACA 5 Digit Number. "
                "The first Number of the series should be 2")
            return 0
        else:
            xf = p / 20
            chord = self.chord
            m = self.camber(xf)
            q = (3 * m - 7 * m ** 2 + 8 * m ** 3 - 4 * m ** 4) / np.sqrt(
                m * (1 - m))
            k1 = 6 * cli / q
            q = q - ((1 - 2 * m) * (np.pi - np.arcsin((1 - 2 * m))) * 1.5)
            if q1 == 0.0:
                for i in range(0, nump / 2):
                    x = self.chord * 0.5 * (
                        np.cos(i * radian * (360.0 / nump)) + 1)
                    xc = x / chord
                    if xc <= m:
                        yc_c = (k1 / 6) * (
                            xc ** 3 - (3 * m * xc ** 2) + m ** 2 * (3 - m) * xc)
                        slope = (k1 / 6) * (
                            3 * xc ** 2 - 6 * m * xc + m ** 2 * (3 - m))
                        theta = np.arctan(slope)
                    else:
                        yc_c = m ** 3 * (1 - (x / chord)) * (k1 / 6)
                        slope = -k1 * m ** 3 / 6
                        theta = np.arctan(slope)
            elif q1 == 1.0:
                if xf == 0.10:
                    m = 0.130
                    k1 = 51.99
                    k1k2 = 0.000764
                elif xf == 0.15:
                    m = 0.2170
                    k1 = 15.793
                    k1k2 = 0.00677
                elif xf == 0.20:
                    m = 0.3180
                    k1 = 6.520
                    k1k2 = 0.0303
                elif xf == 0.25:
                    m = 0.4410
                    k1 = 3.191
                    k1k2 = 0.135500
                else:
                    print_fail(
                        "Invalid profile for NACA 5 Digit Number. "
                        "The second digit can only range from 2 or 5")
                    return 0
                if k1 != 0.0 and k1k2 != 0.0:
                    for i in range(0, nump):
                        x = chord * 0.5 * (
                            np.cos(i * radian * (360.0 / nump)) + 1)
                        xc = x / chord
                        if xc <= m:
                            yc_c = (k1 / 6.0) * ((xc - m) ** 3 - k1k2 *
                                                 (1 - m) ** 3 * xc - m ** 3 *
                                                 xc + m ** 3)
                            slope = 3 * xc ** 2 - xc * 6.0 * m + 3 * m ** 2 - \
                                    k1k2 * (1 - m) ** 3 - m ** 3 * (k1 / 6)
                            theta = np.arctan(slope)
                        else:
                            yc_c = (k1 / 6) * (k1k2 * (xc - m) ** 3) - \
                                   k1k2 * (
                                              1 - m) ** 3 * xc - m ** 3 * xc + m ** 3
                            slope = (
                                k1k2 * (
                                    3 * xc ** 2 - xc * 6.0 * m + 3 * m ** 2 -
                                    k1k2 * (1 - m) ** 3 - m ** 3) * (k1 / 6))
                            theta = np.arctan(slope)

        return yc_c, theta
