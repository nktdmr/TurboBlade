"""
Gas Turbine Engine Design Process

Market Research -> Specification <- Customer Requirements
                        V
               Preliminary Studies:
                Choice of Cycles:
           Type of turbo-machinery layout
                        V
            Thermodynamics design Point
                    studies
                        V
            Aerodynamics of Compressor,
            Turbine, inlet, nozzle, etc.
                        V
                Mechanical Design:
                Stressing of Disks
                Blades, Casing,
                Vibration, Whirling,
                Bearings
                        V
                Detail Design and
                  Manufacturing
                       V
               Test and Development
                       V
                   production
"""

import numpy as np
from math import sqrt

from .turbine.turbine import Turbine
from .compressor.compressor import Compressor
from ..operating.gas import Gas, Gas_Type
from ..operating.atmosphere import Atmosphere


class Gas_Turbine(object):
    """
    generic class for gas Turbine
    """
    pass


class TurboJet(Gas_Turbine):
    """
    generic class for turbojet engine without fan
    """
    pass


class TurboFan(Gas_Turbine):

    def __init__(self, **kwargs):
        """
        Turbo Fan Class Consist of all the components

        Args:
            kwargs: High Level Input Data from Air-framer
            fan_dia: Fan Diameter
            max_thrust: Take off Thrust
            core_thrust: Hot Thrust
            axial_velocity: Inlet Axial Velocity or Take of Velocity
            engine_arch: Engine Architecture
            bpr: By Pass Ratio
            altitude: Operating Point of aircraft mostly design to operate at
                sea level for max thrust
        """
        self.fan_dia = kwargs.get('fan_dia')
        fan_radius = self.fan_dia / 2.0
        self.max_thrust_required = kwargs.get('max_thrust')
        self.core_thrust_factor = kwargs.get('core_thrust')
        self.axial_velocity = kwargs.get('axial_velocity')
        self.engine_architecture = kwargs.get('engine_arch')
        self.bpr = kwargs.get('bpr')
        operating_ht = kwargs.get('altitude')
        if operating_ht is None:
            # at sea level
            operating_ht = 0.0
        self.pressure, self.density, self.temperature = Atmosphere(operating_ht)
        self.mass_flow_rate = np.pi * fan_radius ** 2 * self.density * \
                              self.axial_velocity
        dry_air = Gas_Type(cp=1005.0, cv=718.00)
        self.gas_air = Gas(dry_air)
        # Not using it for now,  required for Turbines and Combustion Chamber
        fuel_air = Gas_Type(cp=0, cv=0)
        self.gas_fuel = Gas(fuel_air)
        self.compressor = Compressor()
        self.turbine = Turbine()

    def get_engine_stations(self):
        """
        Returns all the Major stations
        Returns:

        """
        return

    @property
    def engine_arch(self):
        return


    @property
    def core_mfr(self):
        """
        Mass Flow Rate inside the Core of TurboFan
        Returns: Mass flow rate kg/sec

        """
        return self.mass_flow_rate / self.bpr

    @property
    def by_pass_mfr(self):
        """
        Mass Flow Rate inside by-pass Duct
        Returns: Mass Flow rate Kg/sec

        """
        return self.mass_flow_rate - self.core_mfr

    @property
    def cold_thrust(self):
        """
        Thrust Produced by Fan rate of change of momentum
        Returns: Fan Thrust in Newton

        """
        return self.max_thrust_required * (1 - self.core_thrust_factor)

    @property
    def hot_thrust(self):
        """
        Thrust Produced by the Core of the engine. Rate of change of momentum
        Returns: Core thrust in Newton

        """
        return self.max_thrust_required * self.core_thrust_factor

    @staticmethod
    def pressure_thrust_core(exit_pressure, inlet_pressure, exit_area):
        """
        Thrust produced due to in_pressure difference from core of the engine
        Args:
            exit_pressure (float): Pressure at Nozzle exit
            inlet_pressure (float): Inlet Pressure Ambient Pressure
            exit_area (float): Area of Nozzle at Exit/Last Station

        Returns: Core in_pressure thrust

        """
        return (exit_pressure - inlet_pressure) * exit_area

    @staticmethod
    def pressure_thrust_fan(exit_pressure, inlet_pressure, exit_area):
        """
        Thrust Produced due to in_pressure difference from bypass of the engine
        Args:
            exit_pressure: Pressure at exit of By Pass Duct
            inlet_pressure: Inlet in_pressure or Ambient Pressure
            exit_area: Area of by pass duct at exit.

        Returns: Fan Pressure Thrust
        """
        return (exit_pressure - inlet_pressure) * exit_area

    @property
    def total_thrust(self):
        """
        Total Thrust produced by the Engine
        Returns:

        """
        return self.cold_thrust + \
               self.hot_thrust + \
               self.pressure_thrust_fan(0, 0, 0) + \
               self.pressure_thrust_core(0, 0, 0)

    def installed_thrust(self):
        """
        Thrust produced by the engine after considering the external drag
        Returns: installed thrust

        """
        return self.total_thrust - self.engine_drag

    @property
    def engine_drag(self):
        """
        Drag produced by the engine body after installing
        Returns: Engine drag in Newtons/SI Units

        """
        return self.nacelle_drag + self.additive_drag

    @property
    def nacelle_drag(self):
        """
        Nacelle drag is calculated
        D(nc) = lim (inlet to outlet) integral of (p - po) * Ay
        Returns: Return total drag force

        """
        return 0.0

    @property
    def additive_drag(self):
        """
        Additive drag. The additive drag is the in_pressure drag on the stream
        tube bounding the internal flow between two stations
        Returns:
        """
        return 0.0

    @property
    def req_by_pass_exit_velocity(self):
        """
        Required Exit velocity at the end of By Pass Duct to achieve desired
        thrust will be required to converged the solution of by pass duct design
        Returns: returns velocity in m/sec

        """
        return (self.cold_thrust / self.by_pass_mfr) + self.axial_velocity

    @property
    def req_core_exit_velocity(self):
        """
        Required Exit Velocity at the end of core nozzle to achieve desired
        thrust will be used to converge the solution
        Returns:

        """
        return (self.hot_thrust / self.core_mfr) + self.axial_velocity


class TurboProp(Gas_Turbine):
    pass


class TurboShaft(Gas_Turbine):
    pass
