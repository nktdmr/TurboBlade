import numpy as np
from src.geometry.basic.point import point2D


class rotate2D(object):
    def __init__(self, theta, point=point2D(), arbpoint=point2D()):
        self.theta = theta
        self.point = point
        self.arbPt = arbpoint

    def rotated_point(self):
        m = self.arbPt.x
        n = self.arbPt.y
        p = np.ndarray(shape=3, dtype=float, order='C')
        p[0] = self.point.x
        p[1] = self.point.y
        p[2] = 1.0

        tranform_mat = np.ndarray(shape=(3, 3), dtype=float, order='C')
        tranform_mat[0, 0] = np.cos(self.theta)
        tranform_mat[1, 0] = np.sin(self.theta)
        tranform_mat[2, 0] = 0.0
        tranform_mat[0, 1] = -np.sin(self.theta)
        tranform_mat[1, 1] = np.cos(self.theta)
        tranform_mat[2, 1] = 0.0
        tranform_mat[0, 2] = -m * (np.cos(self.theta) - 1) + n * np.sin(self.theta)
        tranform_mat[1, 2] = -n * (np.cos(self.theta) - 1) - m * np.sin(self.theta)
        tranform_mat[2, 2] = 1.0

        newp = np.dot(p, tranform_mat)
        return newp
