"""
Module for Blade Design
"""
class Blade(object):
    """
    Generic Class Blade Design
    """
    def __init__(self, **kwargs):
        """
        available kwargs:
            nsec: Number of sections from hub to tip usually odd numbers like
                21, 31, 41, 51 etc based on the complexity of blade shape min of
                11
            bia: Blade inlet angles #
            boa: Blade Outlet angles #
            aia: Air inlet angles #
            aoa: Air outlet angles #
            camber: camber angles #
            stagger_angle: Stagger angles angle at which blade is oriented to
                Z-Axis
            aeroblock: contains aero blocks data HubLE(r,x), HubTE(r,x),
                TipLE(r,x), TipTE(r,x)
            airfoils: airfoil sections
        """
        self.n_sec = kwargs.get('nsec')
        self.bia = kwargs.get('bia')
        self.boa = kwargs.get('boa')
        self.aia = kwargs.get('aia')
        self.aoa = kwargs.get('aoa')
        self.camber_angles = kwargs.get('camber')
        self.stagger = kwargs.get('stagger')
        self.aero_block = kwargs.get('aeroblock')
        self.airfoils = kwargs.get('airfoils')

    def meridian_plane(self):
        """
        Curvilinear Plane on which Airfoils are stagged over each other
        Returns:

        """
        pass

    @property
    def stagger_angle(self):
        """

        Returns:

        """
        return

    def radial_distribution(self):
        """

        Returns:

        """
        pass

    def chord_lengths(self, dist=None):
        """
        calculate the chord length for each airfoil section based on linear
        interpolation from Hub to Tip.

        Returns:
            list: list of chord lengths from hub to tip
        """
        return

    def write_blade_file(self):
        """

        Returns:

        """
        pass


class Compressor_Blade(Blade):
    """

    Generic Class for Compressor blades
    """
    pass


class Aspirated_Compressor(Compressor_Blade):
    """
    compressor with aspirated holes from in_pressure surface to suction surface
    to energize boundary layer and delay flow seperation.
    """
    pass


class Turbine_Blade(Blade):
    """

    """
    pass


class Turbine_Cooling_Blade(Turbine_Blade):
    """

    """
    pass


