from Tkinter import *   # from x import * is bad practice
from ttk import *


# http://tkinter.unpythonic.net/wiki/VerticalScrolledFrame

class VerticalScrolledFrame(Frame):
    """A pure Tkinter scrollable frame that actually works!
    * Use the 'interior' attribute to place widgets inside the scrollable frame
    * Construct and pack/place/grid normally
    * This frame only allows vertical scrolling

    """
    def __init__(self, parent, *args, **kw):
        Frame.__init__(self, parent, *args, **kw)

        # create a canvas object and a vertical scrollbar for scrolling it
        vscrollbar = Scrollbar(self, orient=VERTICAL)
        vscrollbar.pack(fill=Y, side=RIGHT, expand=FALSE)
        canvas = Canvas(self, bd=0, highlightthickness=0,
                        yscrollcommand=vscrollbar.set)
        canvas.pack(side=TOP, fill=BOTH, expand=TRUE)
        vscrollbar.config(command=canvas.yview)

        # reset the view
        canvas.xview_moveto(0)
        canvas.yview_moveto(0)

        # create a frame inside the canvas which will be scrolled with it
        self.interior = interior = Frame(canvas)
        interior_id = canvas.create_window(0, 0, window=interior,
                                           anchor=NW)

        # track changes to the canvas and frame width and sync them,
        # also updating the scrollbar
        def _configure_interior(event):
            # update the scrollbars to match the size of the inner frame
            size = (interior.winfo_reqwidth(), interior.winfo_reqheight())
            canvas.config(scrollregion="0 0 %s %s" % size)
            if interior.winfo_reqwidth() != canvas.winfo_width():
                # update the canvas's width to fit the inner frame
                canvas.config(width=interior.winfo_reqwidth())
        interior.bind('<Configure>', _configure_interior)

        def _configure_canvas(event):
            if interior.winfo_reqwidth() != canvas.winfo_width():
                # update the inner frame's width to fill the canvas
                canvas.itemconfigure(interior_id, width=canvas.winfo_width())
        canvas.bind('<Configure>', _configure_canvas)


if __name__ == "__main__":

    class ParameterEditor(Tk):
        def __init__(self, *args, **kwargs):
            self.root = Tk.__init__(self, *args, **kwargs)
            self.build()
            self.count = 1

        @property
        def tittles(self):
            return [" ", "Title", "PType", "Value", "Value Type",
                    "Base Line", "Lower Bound", "Upper Bound", "Expression",
                    "Remove"]

        @property
        def label(self):
            return {}

        def build(self):
            self.build_header()
            self.build_body()
            self.build_footer()

        def build_header(self):
            frame = Frame(self.root)
            frame.pack(side=TOP, fill=X)

            lbl_tittle = Label(frame, text="GPDS Parameter", font=("Arial", 18))
            lbl_tittle.pack(side=LEFT, padx=10, pady=20)

            btn_Add_parameter = Button(frame, text="Add Parameter", width=20,
                                       command=self.add_new)
            btn_Add_parameter.pack(side=RIGHT, padx=10, pady=20)

        def add_new(self):

            lbl_sno = Label(self.v_frame.interior, text=str(self.count) + ')')
            lbl_sno.grid(row=self.count, column=0, sticky=W)

            txt_title = Entry(self.v_frame.interior)
            txt_title.grid(row=self.count, column=1, pady=2, sticky=W+E)

            btn_ptype = Button(self.v_frame.interior, text='PType')
            btn_ptype.grid(row=self.count, column=2, pady=2, sticky=W)

            txt_value = Entry(self.v_frame.interior, text='Value')
            txt_value.grid(row=self.count, column=3, pady=2, sticky=W)

            btn_vtype = Button(self.v_frame.interior, text='Value Type')
            btn_vtype.grid(row=self.count, column=4, pady=2, sticky=W)

            txt_bline = Entry(self.v_frame.interior, text='Base Line')
            txt_bline.grid(row=self.count, column=5, pady=2, sticky=W)


            txt_lbound = Entry(self.v_frame.interior, text='Lower Bound')
            txt_lbound.grid(row=self.count, column=6, pady=2, sticky=W)

            txt_ubound = Entry(self.v_frame.interior, text='Upper Bound')
            txt_ubound.grid(row=self.count, column=7, pady=2, sticky=W)

            txt_exp = Entry(self.v_frame.interior, text='Expression')
            txt_exp.grid(row=self.count, column=8, pady=2, sticky=W+E)

            btn_remove = Button(self.v_frame.interior, text='Remove')
            btn_remove.grid(row=self.count, column=9, pady=2, sticky=W)

            self.v_frame.interior.rowconfigure(self.count,weight=1)
            self.v_frame.interior.columnconfigure(1, weight=3)
            self.v_frame.interior.columnconfigure(2, weight=1)
            self.v_frame.interior.columnconfigure(3, weight=1)
            self.v_frame.interior.columnconfigure(4, weight=1)
            self.v_frame.interior.columnconfigure(5, weight=1)
            self.v_frame.interior.columnconfigure(6, weight=1)
            self.v_frame.interior.columnconfigure(7, weight=1)
            self.v_frame.interior.columnconfigure(8, weight=3)
            self.v_frame.interior.columnconfigure(9, weight=1)
            self.count += 1




        def build_body(self):
            self.v_frame = VerticalScrolledFrame(self.root)
            self.v_frame.pack(side=TOP, fill=X)
            self.frame = Frame(self.v_frame)
            self.frame.pack(side=TOP, fill=X)
            label = {}
            i = 0

            for title in self.tittles:
                lb = Label(self.v_frame.interior, text=title)
                lb.grid(row=0, column=i, sticky=W)
                label[title] = lb
                self.v_frame.interior.columnconfigure(i, weight=1)
                i += 1

            self.v_frame.interior.rowconfigure(0, weight=1)
            self.v_frame.interior.columnconfigure(0, weight=0)
            self.v_frame.interior.columnconfigure(1, weight=3)
            self.v_frame.interior.columnconfigure(8, weight=3)
            self.v_frame.interior.columnconfigure(9, weight=0)





        def build_footer(self):
            frame = Frame(self.root)
            frame.pack(side=BOTTOM, fill=X)

            lbl_tittle = Label(frame, text="Error Message:")
            lbl_tittle.pack(side=LEFT, padx=10, pady=20)

            btn_cancel = Button(frame, text="Cancel", width=20)
            btn_cancel.pack(side=RIGHT, padx=10, pady=20)

            btn_ok = Button(frame, text="O.K", width=10)
            btn_ok.pack(side=RIGHT, padx=10, pady=20)









    app = ParameterEditor()
    app.mainloop()