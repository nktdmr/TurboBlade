import matplotlib.pyplot as plt
import numpy as np
from src.geometry.transformation.rotation import rotate2D
from src.geometry.basic.point import point2D


def plot(cords, scaley=True, scalex=True, y_label='y-axis', x_label='x-axis',
         color='blue', title='', graph_type='-o'):
    """
    :param title:
    :param graph_type:
    :param color:
    :param cords:
    :param scaley:
    :param scalex:
    :param y_label:
    :param x_label:
    :return:
    """
    plt.plot(cords[0][:-1], cords[1][:-1], graph_type, scaley=scaley,
             scalex=scalex, color=color, label="Pressure Graph")
    plt.grid(True)
    plt.ylabel(y_label)
    plt.xlabel(x_label)
    plt.title(title)
    plt.gca().set_aspect('auto')
    plt.show()


def plot_airfoil_with_cp(cords, cp, scale_y=True):
    plt.plot(cords[0][:-1], cords[1][:-1])
    plt.grid(True)
    n = len(cp)

    for i in range(0, n - 1):
        rgb = getRGB(min(cp[:-1]), max(cp[:-1]), cp[i])
        if i <= (n + 2) / 2:
            theta = -(np.pi / 2) - np.arctan2(cords[1][i + 1] - cords[1][i],
                                              cords[0][i + 1] - cords[0][i])
        else:
            theta = (np.pi / 2) - np.arctan2(cords[1][i + 1] - cords[1][i],
                                             cords[0][i + 1] - cords[0][i])
        # print np.arctan2(cords[1][i+1] - cords[1][i], cords[0][i+1] -
        # cords[0][i])
        point = point2D(x=cp[i])
        arbpt = point2D(x=cords[0][i], y=cords[1][i])
        newp = rotate2D(theta, point, arbpt).rotated_point()
        plt.arrow(cords[0][i], cords[1][i], newp[0] / 5.0, newp[1] / 5.0,
                  head_width=0.003, head_length=0.005, color=rgb)
        # ax.arrow(0, 0, 0.05, 0.04, head_width=0.005, head_length=0.005,
        # fc='k', ec='k')
    plt.gca().set_aspect('equal')
    plt.figaspect(1.0)

    plt.show()


def getRGB(min1, max1, value):
    per = (value - min1) / (max1 - min1)
    if per <= 0.25:
        g = 0
        b = 1
        r = 0
        g += per / 0.25
    elif 0.50 >= per > 0.25:
        b = 1
        g = 1
        r = 0
        b -= (per - 0.25) / 0.25
    elif 0.75 >= per > 0.5:
        r = 0
        b = 0
        g = 1
        r += (per - 0.5) / 0.25
    else:
        g = 1
        r = 1
        b = 0
        g -= (per - 0.75) / 0.25

    return r, g, b, 1.0
