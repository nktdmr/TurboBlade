# coding=utf-8

import struct
import sys


from colorama import Fore, Back, Style


def reset():
    print(Style.RESET_ALL)


def print_magenta_high(prt):
    print(Back.MAGENTA + prt + Style.RESET_ALL)
    return


def pr_red(prt):
    """
    :param prt:
    """
    print(Fore.RED + prt + Style.RESET_ALL)
    return


def pr_green(prt):
    """
    :param prt:
    """
    print(Fore.GREEN + prt + Style.RESET_ALL)
    return


def pr_yellow(prt):
    """
    :param prt:
    """
    print(Fore.YELLOW + prt + Style.RESET_ALL)
    return


def pr_light_cyan(prt):
    """
    :param prt:
    """
    print(Fore.LIGHTCYAN_EX + prt + Style.RESET_ALL)
    return


def pr_cyan(prt):
    """
    :param prt:
    """
    print(Fore.CYAN + prt + Style.RESET_ALL)
    return


def pr_light_gray(prt):
    """
    :param prt:
    """
    print(Fore.LIGHTBLACK_EX + prt + Style.RESET_ALL)
    return


def pr_black(prt):
    """
    :param prt:
    """
    print(Fore.BLACK + prt + Style.RESET_ALL)
    return


def pr_blue(prt):
    """
    :param prt:
    """
    print(Fore.BLUE + prt + Style.RESET_ALL)
    return


def print_ok(prt):
    """
    :param prt:
    """
    pr_green(prt)


def print_warn(prt):
    """
    :param prt:
    """
    pr_yellow(prt)


def print_highlight(prt):
    print(Back.GREEN + Style.BRIGHT + Fore.BLUE + prt + Style.RESET_ALL)


def print_fail(prt):
    """
    :param prt:
    """
    print(Style.BRIGHT + Back.RED + Fore.BLUE + prt + Style.RESET_ALL)


def print_header(prt):
    """
    :param prt:
    """
    print(Style.BRIGHT + Fore.CYAN + prt + Style.RESET_ALL)


def print_bold(prt):
    """
    :param prt:
    """
    print(Style.BRIGHT + prt + Style.RESET_ALL)
    return


def print_underline(prt):
    """
    :param prt:
    """
    print(Style.BRIGHT + Back.BLUE + prt + Style.RESET_ALL)


def getTerminalSize():
    """
    :return:
    """
    import os
    env = os.environ

    def ioctl_GWINSZ(fd):
        """
        :param fd:
        :return:
        """
        try:
            import termios
            import fcntl
            cr = struct.unpack('hh', fcntl.ioctl(fd, termios.TIOCGWINSZ, '1234'))
        except:
            return
        return cr

    cr = ioctl_GWINSZ(0) or ioctl_GWINSZ(1) or ioctl_GWINSZ(2)
    if not cr:
        try:
            fd = os.open(os.ctermid(), os.O_RDONLY)
            cr = ioctl_GWINSZ(fd)
            os.close(fd)
        except:
            pass
    if not cr:
        cr = (env.get('LINES', 25), env.get('COLUMNS', 80))

    return int(cr[1]), int(cr[0])


# Print iterations progress
def print_progress_bar(iteration, total, prefix='', suffix='',
                       decimals=3, length=100, fill=Back.BLUE + ' ' + Style.RESET_ALL,
                       fit_to_console=True):
    """"
    Call in a loop to create terminal progress bar
    :param iteration: - Required  : current iteration (Int)
    :param total: - Required  : total iterations (Int)
    :param prefix: - Optional  : prefix string (Str)
    :param suffix: - Optional  : suffix string (Str)
    :param decimals: - Optional  : positive number of decimals in percent complete (Int)
    :param length: - Optional  : bar fill character (Str)
    :param fill: - Optional  : bar fill character (Str)
    :param fit_to_console: Fit the progress bar to console
    :return:
    """

    (width, height) = getTerminalSize()
    if fit_to_console:
        length = width - len(prefix) - len(suffix) - 9
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filled_length = int(length * iteration // total)
    bar = fill * filled_length + '|' * (length - filled_length)
    sys.stdout.write("\r%s |%s| %d%% %s" % (prefix, bar, float(percent), suffix))
    sys.stdout.flush()
    # Print New Line on Complete
    if iteration == total:
        print (" \n")
    return


def print_cfd():
    """
    :return:
    """
    pr_green('''
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++    
+         _________       __________        ________            +
+        / _______/      /  ________/    D / _______\           +
+       / /              | /             Y | |     \ \          +
+      | |               | |______       N | |      \ \         +
+      | |               |  ______|      A | |       | |        +
+      | |COMPUTATIONAL  | |FLUID        M | |       | |        +
+      | |               | |             I | |      / /         +
+       \ \_______       | |             C | |_____/ /          +
+        \________/      |_|             S  \_______/           +
+                                                               + 
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++      
     ''')
    return


def print_turbo():
    """
    :return:
    """
    pr_green('''
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++  
+        ########  ##   ##  ######   #####     #####            +
+           ##     ##   ##  ##   ##  ##   ##  ##   ##           +
+           ##     ##   ##  #####    #####    ##   ##           +
+           ##     ##   ##  ## ##    ##   ##  ##   ##           +
+           ##      #####   ##  ###  ######    #####            +
+                                                               +
+    #####   ##      ##    #####   ###### ###    ##   ######    +
+    ##   ## ##     ## ##  ##   ##   ##   ####   ##  ##         +
+    ######  ##    ####### ##   ##   ##   ## ##  ##  ## ####    +
+    ##   ## ##    ##   ## ##   ##   ##   ##  ## ##  ##   ##    +
+    #####   ##### ##   ## #####   ###### ##   ####   ######    +
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    ''')
    return


def print_engine_graphics():
    """
    :return:
    """
    print_ok('''
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+         ___________                                           +
+        /       |   |                                          +
+        |      |    |                                          + 
+        |     |     |                       ________           +
+        |    | FAN  |                      /________\          +
+        |   |_______\                     / _____    \         +
+        |   /        \                   / /     \    \        +
+       /|  /          \_________________/ /       \    \       +
+      / | /                   TURBO                \    \      +
+     ( <|( COMPRESSOR--------BLADING-------TURBINE  )>   )     +
+      \ | \            _________________           /    /      +
+       \|  \          /     COMBUSTOR   \ \       /    /       +
+        |   \_______ /                   \ \_____/    /        +
+        |   |       /                     \__________/         +
+        |    | FAN  |                      \________/          +
+        |     |     |                                          +
+        |      |    |                                          +
+        \_______|___|                                          +
+                                                               +     
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    ''')
    return


def print_copyright():
    """
    :return:
    """
    print_ok(
        '''
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+            Copyright (c) 2016-2018, Ankit Dimri               +
+                email: ankitdmr1@gmail.com                     +
+                 Copyright (c) 2017-2018                       +
+                                                               +
+   Redistribution and use in source and binary  forms, with    +
+   or without modification, are permitted provided that the    + 
+   following conditions are met:                               +
+                                                               +
+                                                               +
+    * Redistributions of source code must retain the above     +
+        copyright notice, this list of  conditions and the     +
+        following disclaimer.                                  +
+                                                               +            
+   * Redistributions in binary form must reproduce the         +
+       above copyright notice, this list of  conditions and    +
+       the following disclaimer in the documentation and/or    +
+       other materials provided with the distribution.         +
+                                                               +
+   * Neither the name of the copyright holders nor the names   +
+       of any contributors may be used to endorse or promote   +
+       products  derived from this software without specific   +
+       prior written permission.                               +
+                                                               +
+   THIS  SOFTWARE  IS  PROVIDED  BY THE COPYRIGHT HOLDERS AND  +
+   CONTRIBUTORS   "AS   IS"  AND  ANY   EXPRESS   OR IMPLIED,  +
+   WARRANTIES INCLUDING, BUT  NOT  LIMITED  TO,  THE  IMPLIED  +
+   WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR  +
+   PURPOSE ARE  DISCLAIMED. IN NO EVENT  SHALL THE  COPYRIGHT  +
+   OWNER OR CONTRIBUTORS  BE LIABLE FOR ANY DIRECT, INDIRECT,  + 
+   INCIDENTAL,  SPECIAL, EXEMPLARY,  OR CONSEQUENTIAL DAMAGES  + 
+   (INCLUDING, BUT NOT LIMITED TO,  PROCUREMENT OF SUBSTITUTE  + 
+   GOODS  OR  SERVICES;  LOSS  OF  USE, DATA, OR  PROFITS; OR  +
+   BUSINESS INTERRUPTION) HOWEVER  CAUSED AND ON  ANY  THEORY  +
+   OF LIABILITY, WHETHER IN CONTRACT,  STRICT  LIABILITY,  OR  +
+   TORT(INCLUDING  NEGLIGENCE OR  OTHERWISE)  ARISING IN  ANY  +
+   WAY OUT OF THE USE OF THIS SOFTWARE, EVEN  IF  ADVISED  OF  +
+   THE POSSIBILITY OF SUCH DAMAGE.                             +
+                                                               +
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
''')
    return
