import xml.etree.ElementTree as Et


class XMLParser(object):
    """
    Class for xml parser
    """

    def __init__(self, filename=None, root=None):
        """
        XML Parser Constructor
        Args:
            filename (str): File Name
            root (str): Root Name
        """
        if filename is None:
            if root is not None:
                self.xml = Et.ElementTree(element=Et.Element(root))
            else:
                self.xml = Et.ElementTree(element=Et.Element("input"))
        else:
            try:
                self.xml = Et.parse(filename)
            except Exception as er:
                raise LookupError("Invalid XML File, {}".format(str(er)))

        self.root = self.xml.getroot()
        pass

    def is_valid_xml(self):
        """
        Returns: true or false
        """
        pass

    def get_parent(self, element):
        """
        Get the parent of child node
        Args:
            element (Et.Element): Child node

        Returns (Et.Element): Parent element of the child node

        """
        parent_map = {c: p for p in self.xml.iter() for c in p}
        return parent_map.get(element)

    def get_parents(self, element):
        """
        Get parent node of a element, Recursive function can be used to get
        all the parents of a child element
        Args:
            element (Et.Element): element which is teh child node and need a
            parent node
            of it. the element is a unique element pointing towards a
            specific memory location

        Returns (list): Returns the list of all the parents till root

        """
        parents = []

        def _get_parent(_element):

            if self.get_parent(_element) is not self.root:
                parents.append(self.get_parent(_element))
                _get_parent(self.get_parent(_element))

        _get_parent(element)

        return parents

    def get_xpath_of_element(self, element):
        """
        Get the xpath of the element supplied
        Args:
            element (Et.Element): Element

        Returns (str): xpath of the element

        """
        assert isinstance(element, Et.Element)

        list_of_parent = self.get_parents(element)
        tags = []
        for elm in list_of_parent:
            tags.append(elm.tag)

        tags.reverse()
        tags.append(element.tag)
        return "./" + "/".join(tags)

    def get_siblings(self, element):
        """
        Return all the siblings of element with same name
        Args:
            element (Et.Element):

        Returns (list): list of elements

        """
        assert isinstance(element, Et.Element)
        return self.get_nodes(self.get_xpath_of_element(element))

    def get_neighbours(self, element):
        """
        Returns all the element which share same immediate parent.
        Args:
            element (Et.Element): Element

        Returns (list): returns the list of all the element which share same
        parent

        """
        assert isinstance(element, Et.Element)
        parent = self.get_parent(element)
        return list(parent)

    @staticmethod
    def is_valid_xpath(xpath):
        """
        Valid File:
            Element tags start with all Alphabets
            Element can contain a underscore '_'
        Invalid File:
            Element tag cannot start with a digit
            Element tag cannot contain any special char
            Xpath cannot start with a root that is '.'
            Xpath should contain at-least one forward slash i.e '/'
        Args:
            xpath: path for node

        Returns: True or False
        """
        invalid_chars = list(map(chr, range(32, 48))) + \
                        list(map(chr, range(91, 95))) + \
                        list(map(chr, range(123, 128))) + \
                        list(map(chr, range(59, 65)))

        digits = list(map(chr, range(48, 58)))

        if not isinstance(xpath, str):
            return False

        if xpath[0] is not ".":
            return False
        elif "/" in xpath:
            for each in xpath.split('/')[1:]:
                if each[0] in digits:
                    return False
                elif any(i in each for i in invalid_chars):
                    return False
        elif "/" not in xpath:
            return False

        return True

    def get_nodes(self, xpath, top_node=None):
        """
        Get Nodes
        Only takes valid xpath, Refer Valid Xpath Docs for details
        Args:
            xpath: xpath in xml file to the element
            top_node: parent node of the path, in case of relative paths

        Returns: Single node or List of Nodes
        """
        self._raise_exception_if_invalid(xpath)
        if top_node is None:
            top_node = self.root

        nodes = top_node.findall(xpath)
        if len(nodes) == 1:
            return nodes[0]
        elif len(nodes) == 0:
            return None
        return nodes

    def get_node(self, xpath, top_node=None, index=0):
        """
        return the node with the help of xpath and index supplied
        Args:
            xpath (str): xpath is teh path the node which needs to returned
            top_node (Et.Element): Root node/ parent node of the xpath
            index (int): position of the node from the beginning if the node
            exist more than once, 0 index will return the first node
            appearing in the xml and like wise

        Returns (Et.Element): The element at xpath with index provided

        """
        if top_node is None:
            top_node = self.root

        return top_node.findall(xpath)[index]

    def get_value(self, xpath, top_node=None):
        """
        get value of node which is found first in xml
        think about how to manage with multiple similar nodes

        return type casted value of the value, if type attribute is not
        available than return string
        Args:
            xpath (str):
            top_node (Et.Element):

        Returns: returns the first found node in sequence
        """
        self._raise_exception_if_invalid(xpath)

        if top_node is None:
            top_node = self.root

        nodes = top_node.findall(xpath)
        if nodes[0].attrib.get("type") is not None:
            typeof = nodes[0].attrib["type"]
        else:
            typeof = "str"

        ret, value = self._get_type_casted_value(nodes[0].text, typeof)
        if ret:
            return value
        else:
            raise TypeError("TypeCastError" + str(value))

    @staticmethod
    def _get_type_casted_value(string, typeof):
        """
        Type cast the value of node to the desired type mentioned in attribute
        Default type of value is string
        # TODO: implement unit conversion if values are float or int or arrays

        Args:
            typeof (str): the type in which value needs to typecasted
            string (str): the value in which needs to type casted

        Returns: True of False and type casted value
        """
        valid_types = {"str": str,
                       "int": int,
                       "int_array": list,
                       "str_array": list,
                       "float": float,
                       "float_array": list,
                       "xpath": str,
                       "bool": bool}

        yeses = ['true', '1', 'yes', 'haan']
        noes = ['false', '0', 'no', 'nahi', 'na', 'nein']

        if not isinstance(string, str):
            return 0, "Not a string"

        assert isinstance(string, str)
        assert isinstance(typeof, str)

        try:
            if valid_types[typeof] in [int, float]:
                return 1, valid_types[typeof](string)
            elif valid_types[typeof] is list:
                if "," in string:
                    lst = string.split(',')
                else:
                    lst = string.split()
                if "int_" in typeof:
                    return 1, list(map(int, lst))
                elif "float_" in typeof:
                    return 1, list(map(float, lst))
                return 1, lst
            elif valid_types[typeof] is bool:
                if string.lower() in yeses:
                    return 1, True
                if string.lower() in noes:
                    return 1, False
            else:
                return 1, string
        except Exception as er:
            return 0, er

    def _raise_exception_if_invalid(self, xpath):
        """
        Raise a lookup error when a invalid xpath is being used
        Args:
            xpath: xpath which needs to be checked, if valid do nothing and
            if not raise exception

        Returns:

        """
        if not self.is_valid_xpath(xpath):
            raise LookupError('Invalid Xpath: {} Please Check Supplied '
                              'XPATH'.format(xpath))

    def get_attributes(self, xpath, top_node=None):
        """
        get the dictionary of arguments from the supplied xpath

        Args:
            xpath (str): path to the node or element
            top_node (Et.Element): Parent node relative to the path

        Returns (dict): Dictionary of attributes

        """
        self._raise_exception_if_invalid(xpath)
        if top_node is None:
            top_node = self.root
        if isinstance(top_node, Et.Element):
            nodes = top_node.findall(xpath)
        else:
            raise TypeError("Top Node Supplied is of Invalid Data Type {} is "
                            "not a ET.Element".format(str(type(top_node))))
        # consider only unique node

        attrib = nodes[0].attrib
        return attrib

    def get_value_attribute(self, xpath, key, top_node=None):
        """

        Args:
            xpath (str): path to the node or element
            key (str): key to of attribute
            top_node (Et.Element): parent node relative to the path.

        Returns: return the value of attribute with supplied key

        """
        self._raise_exception_if_invalid(xpath)
        if top_node is None:
            top_node = self.root
        if isinstance(top_node, Et.Element):
            nodes = top_node.findall(xpath)
        else:
            raise TypeError("Top Node Supplied is of Invalid Data Type {} is "
                            "not a ET.Element".format(str(type(top_node))))

        attrib = nodes[0].attrib
        value = attrib[key]

        return value

    def has_node(self, xpath, top_node=None):
        """

        Args:
            xpath: path to the node which need to checked for existence
            top_node: Parent node relative to the xpath

        Returns: True or False

        """
        self._raise_exception_if_invalid(xpath)
        if top_node is None:
            top_node = self.root

        nodes = top_node.findall(xpath)

        if len(nodes) >= 1:
            return True

        return False

    def modify_xpath(self, xpath, text=None, attrib=None, top_node=None):
        """
        Modify the existing xpath , if path does not exist create a new
        element at the location provided
        Args:
            xpath: path to the node or element
            text: value to be added as string
            attrib: attributes to add to node
            top_node: parent node of the xpath, parent node is considered as
                the root for the same

        Returns:

        """
        self._raise_exception_if_invalid(xpath)

        if not self.has_node(xpath):
            raise LookupError("Xpath supplied does not exist")

        if top_node is None:
            top_node = self.root

        if isinstance(top_node, Et.Element):
            nodes = top_node.findall(xpath)
        else:
            raise TypeError("Top Node Supplied is of Invalid Data Type {} is "
                            "not a ET.Element".format(str(type(top_node))))
        for node in nodes:
            assert isinstance(node, Et.Element)
            if text is not None:
                node.text = text
            if attrib is not None:
                node.attrib = attrib

    def create_xpath(self, xpath, top_node=None):
        """
        Create Element with supplied xpath
        Args:
            xpath: path to the node or element
            top_node: parent node of the xpath

        Returns: None, Create xpath in the element tree
            modify xpath to add value to it.

        """
        pass

    def remove_element(self, xpath, top_node=None):
        """
        remove elements for a supplied xpath
        Args:
            xpath:  path to node
            top_node: parent node

        Returns: None, Just remove every node with specified xpath within
        topnode

        """
        self._raise_exception_if_invalid(xpath)

        if top_node is None:
            top_node = self.root

        assert isinstance(top_node, Et.Element)

        for node in top_node.findall(xpath):
            parent_node = self.get_nodes("/".join(xpath.split('/')[:-1]))
            parent_node.remove(node)

    def copy_xpath(self, xpath, top_node=None):
        """

        Args:
            xpath:
            top_node:

        Returns:

        """
        pass

    def write(self, filename):
        """

        Args:
            filename:

        Returns:

        """
        pass
