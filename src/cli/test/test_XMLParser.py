import unittest
from src.cli.xml_parser import XMLParser
import xml.etree.ElementTree as Et


class TestXMLParser(unittest.TestCase):
    def setUp(self):
        self.xml_obj = XMLParser(filename='resources/test.xml')
        self.xml_obj_new = XMLParser()

    def test_constructor_xml(self):
        # Test if the constructor is working properly or not
        self.assertTrue(isinstance(self.xml_obj.root, Et.Element))
        self.assertTrue(self.xml_obj.root.tag == "input")
        self.assertTrue(isinstance(self.xml_obj.xml, Et.ElementTree))

    def test_constructor_new_xml(self):
        # Check if the constructor with new object is working or not
        self.assertTrue(isinstance(self.xml_obj_new.root, Et.Element))
        self.assertTrue(self.xml_obj_new.root.tag == "input")
        self.assertTrue(isinstance(self.xml_obj_new.xml, Et.ElementTree))

    def test_invalid_file(self):
        self.assertRaises(Exception, XMLParser, filename='resources/test1.xml')

    def test_is_valid_xpath(self):
        # List of all valid paths append any valid path encountered
        valid_x_paths = ['./airfoil/naca/Title',
                         './input/airfoil/naca/title_new']

        # List of all invalid Paths, Append any invalid Path encountered
        invalid_x_paths = ['./airfoil/naca/title/2D',
                           '/airfoil/naca/title',
                           '.airfoilnacatitle',
                           './air#foil/naca/title',
                           './airfoil/naca/title]',
                           './<airfoil>/{naca}/<title>',
                           './airfoil=/nac.a/title',
                           10]

        for path in valid_x_paths:
            ret = self.xml_obj.is_valid_xpath(path)
            self.assertTrue(ret)

        for path in invalid_x_paths:
            ret = self.xml_obj.is_valid_xpath(path)
            self.assertTrue(ret is False)

    def test_get_nodes(self):
        valid_x_path = './airfoil/naca/Title'
        nodes = self.xml_obj.get_nodes(valid_x_path)
        self.assertTrue(nodes.text == "NACA 4415")
        attrib = nodes.attrib
        self.assertTrue(attrib["type"] == "str" and attrib[
            "description"] == "title of airfoil")
        self.assertTrue(nodes.tag == "Title")
        invalid_xpath = './airfoil/naca/2D'
        self.assertRaises(Exception, self.xml_obj.get_nodes, invalid_xpath)

    def test_get_value(self):
        str_xpath = './airfoil/naca/Title'
        value = self.xml_obj.get_value(str_xpath)
        self.assertTrue(isinstance(value, str))
        self.assertTrue(value == "NACA 4415")

        int_xpath = "./airfoil/naca/LEpts"
        value = self.xml_obj.get_value(int_xpath)
        self.assertTrue(isinstance(value, int))

        float_xpath = "./airfoil/naca/Chord"
        value = self.xml_obj.get_value(float_xpath)
        self.assertTrue(isinstance(value, float))

        invalid_xpath = "/airfoil/naca/chord"
        self.assertRaises(Exception, self.xml_obj.get_value, invalid_xpath)

        float_xpath = ["./blade/float_list",
                       "./blade/float_list2",
                       "./blade/float_list3"]

        for xpath in float_xpath:
            value = self.xml_obj.get_value(xpath)
            self.assertTrue(isinstance(value, list))
            self.assertTrue(all(isinstance(x, float) for x in value))

        list_xpath = "./blade/int_list"
        value = self.xml_obj.get_value(list_xpath)
        self.assertTrue(isinstance(value, list))
        self.assertTrue(all(isinstance(x, int) for x in value))

    def test_get_attributes(self):

        xpath = "./blade/float_list"
        self.assertRaises(TypeError, self.xml_obj.get_attributes, xpath,
                          top_node=10)
        attrib = self.xml_obj.get_attributes(xpath)
        self.assertTrue(isinstance(attrib, dict))
        self.assertTrue(attrib["type"] == "float_array")

    def test_get_value_attribute(self):
        xpath = "./blade/float_list"
        value = self.xml_obj.get_value_attribute(xpath, "type")
        self.assertTrue(value == "float_array")

    def test_has_node(self):
        xpath = "./blade/float_list"
        self.assertTrue(self.xml_obj.has_node(xpath))

    def test_modify_xpath(self):
        xpath = './airfoil/naca/Title'
        self.xml_obj.modify_xpath(xpath, text="NACA 2315",
                                  attrib={"type": "str"})
        value = self.xml_obj.get_value(xpath)
        self.assertTrue(value == "NACA 2315")

    def test_create_xpath(self):
        pass

    def test_remove_element(self):
        xpath = './airfoil/uiuc/location'
        self.xml_obj.remove_element(xpath)
        value = self.xml_obj.get_nodes(xpath)
        self.assertTrue(value is None)

    def test_get_parent(self):
        element = self.xml_obj.get_nodes('./airfoil/naca/Title')
        parent = self.xml_obj.get_parent(element)
        self.assertTrue(parent.tag == 'naca')

    def test_get_parents(self):
        element = self.xml_obj.get_nodes('./airfoil/naca/Title')
        elements_list = self.xml_obj.get_parents(element)
        self.assertTrue(isinstance(elements_list, list))

    def test_get_xpath_of_element(self):
        _xpath = './airfoil/naca/Title'
        element = self.xml_obj.get_nodes(_xpath)
        xpath = self.xml_obj.get_xpath_of_element(element)
        self.assertTrue(xpath == _xpath)

    def test_get_neighbours(self):
        _xpath = './airfoil/naca/Title'
        element = self.xml_obj.get_nodes(_xpath)
        neighbours = self.xml_obj.get_neighbours(element)
        self.assertTrue(isinstance(neighbours, list))

    def test_get_siblings(self):
        _xpath = './doe/study/design_parameter'
        element = self.xml_obj.get_nodes(_xpath)
        siblings = self.xml_obj.get_siblings(element[0])
        self.assertTrue(isinstance(siblings, list))

    def test_copy_xpath(self):
        pass

    def test_write(self):
        pass


if __name__ == '__main__':
    unittest.main()
