from distutils.core import setup

setup(
    name='CFD Project',
    version='0.1',
    packages=['src', 'src.cli', 'src.solver', 'src.solver.DNS', 'src.solver.FDM', 'src.solver.FEM',
              'src.solver.FVM', 'src.solver.RANS', 'src.solver.eulers', 'src.solver.throughflow',
              'src.solver.navierstokes', 'src.solver.potentialflow', 'src.meshing', 'src.geometry', 'src.geometry.disc',
              'src.geometry.blade', 'src.geometry.shaft', 'src.geometry.cavity', 'src.geometry.annulus',
              'src.geometry.turbine', 'src.geometry.airfoils', 'src.geometry.airfoils.NACA',
              'src.geometry.airfoils.UIUC', 'src.geometry.combustor', 'src.geometry.compressor',
              'src.geometry.installation', 'src.geometry.installation.inlet', 'src.geometry.installation.exhaust',
              'src.geometry.transformation', 'src.visualization'],
    url='git@gitlab.com:nktdmr/TurboBlade.git',
    license='Open Source-Apace.20',
    author='ankit',
    author_email='ankitdmr1@gmail.com',
    description='Welcome to TurboBlading', requires=['matplotlib', 'numpy', 'sympy', 'scipy']
)
