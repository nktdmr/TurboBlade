
from src.geometry.airfoils.NACA import naca4digit as n4
from src.visualization import basicplot as bp
from src.cli.cli import *
from src.solver.potentialflow.potentialflow import PanelMethod
from src.solver.potentialflow.potentialflow import RunAnalysis
from src.cli.xml_parser import XMLParser

print_copyright()
print_engine_graphics()
xml = XMLParser(r'D:\Project\TurboBlade\input\input.xml')
naca_digit = xml.get_value('./airfoil/naca/digit')
chord = xml.get_value('./airfoil/naca/chord')
airfoil = n4.NACA4Digit(str(naca_digit))

pt_solve = PanelMethod(airfoil=airfoil)

bp.plot(pt_solve.cpr,
        x_label='Chord',
        y_label='Coefficient of Pressure',
        graph_type='-o')

bp.plot_airfoil_with_cp(pt_solve.airfoil_cords, pt_solve.cpr[1])
pt_solve.print_results()

_start_aoa = xml.get_value('./solver/potential_flow/analysis/start_aoa')
_end_aoa = xml.get_value('./solver/potential_flow/analysis/end_aoa')
_step = xml.get_value('./solver/potential_flow/analysis/step')

results_graphs = RunAnalysis(start_aoa=_start_aoa, end_aoa=_end_aoa, step=_step,
                             pr=False).analysis_results()

clAlpha = (results_graphs[4], results_graphs[0])
cdAlpha = (results_graphs[4], results_graphs[1])
cmAlpha = (results_graphs[4], results_graphs[2])
cpAlpha = (results_graphs[4], results_graphs[3])
cl_cd = (results_graphs[1], results_graphs[0])

bp.plot(clAlpha,
        x_label="Angle of Attack",
        y_label="Lift Coefficient",
        color='red',
        title='Cl Vs Alpha',
        graph_type='-o')

bp.plot(cdAlpha,
        x_label="Angle of Attack",
        y_label="Drag Coefficient",
        color='green',
        title='Cd Vs Alpha',
        graph_type='-d')

bp.plot(cmAlpha,
        x_label="Angle of Attack",
        y_label="Moment Coefficient",
        color='cyan',
        title='Cm Vs Alpha',
        graph_type='->')

bp.plot(cl_cd,
        x_label="Drag Coefficient",
        y_label="Lift Coefficient",
        color='magenta',
        title='Cl Vs Cd',
        graph_type='-<')

bp.plot(cpAlpha,
        x_label="Angle of Attack",
        y_label="Min Coefficient of Pressure",
        color='yellow',
        title='Cp Vs Alpha',
        graph_type='-v')

print_turbo()
print_header('Successful Run')
