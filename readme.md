# Welcome to TurboBlade
Complete 3D Visualization for Blading
![Blading_1](results/blading.PNG)


## Example 1: Heat Conduction 2D:

2D Heat Conduction equation is given by:

```math
dT/dt = alpha (d^2 T/ dx^2 + d^2 T/ dy^2)
```

![Heat2D_1](results/Heat 2D 2.mp4)

## Example 2: Heat Conduction 2D:

2D Heat Conduction equation is given by:

```math
dT/dt = alpha (d^2 T/ dx^2 + d^2 T/ dy^2)
```

![Heat2D_2](results/Heat2D_1.mp4)

## Example 3: Burgers 1D Equation

1D Non-Linear convection equation is given by:

```math
dU/dt + U dU/dx = 0
```

![Burgers Equation](results/Burgers Equation.mp4)

## Example 4: Wave 1D Equation

1D Linear convection equation is given by:

```math
dU/dt + C dU/dx = 0
```

![Wave Equation](results/Wave Equation 1D.mp4)

## Example 5: 

1D countour Plots Marching in time

1D Heat Conduction:

![alt text](results/Heat1DPlot.png)

![alt text](results/1.png)
![alt text](results/2.png)
![alt text](results/3.png)
![alt text](results/4.png)
![alt text](results/5.png)
![alt text](results/6.png)
![alt text](results/7.png)



The Zen of Python, by Tim Peters

*	Beautiful is better than ugly.
*	Explicit is better than implicit.
*	Simple is better than complex.
*	Complex is better than complicated.
*	Flat is better than nested.
*	Sparse is better than dense.
*	Readability counts.
*	Special cases aren't special enough to break the rules.
*	Although practicality beats purity.
*	Errors should never pass silently.
*	Unless explicitly silenced.
*	In the face of ambiguity, refuse the temptation to guess.
*	There should be one-- and preferably only one --obvious way to do it.
*	Although that way may not be obvious at first unless you're Dutch.
*	Now is better than never.
*	Although never is often better than *right* now.
*	If the implementation is hard to explain, it's a bad idea.
*	If the implementation is easy to explain, it may be a good idea.
*	Namespaces are one honking great idea -- let's do more of those!

